---
title: Meetings
---

Meetings are an important aspect of effective collaboration. They are also an expensive investment of teammate time, and (especially importantly) the synchronous time that can be scarce when working across disparate time zones. The following norms aim to establish efficient and effective practices.

## Meeting Norms

### Scheduling meetings

To schedule Zoom meetings from within Google Calendar:

1. Ensure that you have Zoom installed as an add-on to your Google account. Open Google Calendar, click the settings cog in the top right corner, and then select the "Get add-ons" option.
2. Search for the Zoom add-on and click the envelope and calendar combo icon to install. Now you're ready to schedule your first Zoom meeting.
3. Open Google Calendar, tap the "+ Create" icon then Event.
4. Enter the meeting details, set time and time zone (if needed), add invitees, or location
5. Add links to the agenda or documents relevant to the meeting.
5. Tap Add conferencing and select Zoom Meeting (you may be prompted to sign into Zoom). Google Calendar will add a Zoom Meeting to your meeting details.
6. Tap Save.

### Meeting format

1. Meetings must be scheduled using [Google Calendar](https://calendar.google.com) and should have a zoom link included
1. **All meetings** must have an agenda in a shared Google Doc that's linked in the Description. Items for discussion must be listed in the agenda prior to the start of the meeting.
1. If client facing meeting, set time zone to local time to prevent meeting shifts due to day light savings. If internal meeting with multiple team members, then use the time zone for the organizer of the meeting unless needed otherwise.
1. Meeting requests will remain **ON HOLD** if the above format is not completed (Example: missing documentation, agenda, etc)
1. When a meeting has no agenda for a few weeks, it should be reviewed and a decision should be made whether to cancel or restructure it.
1. Meetings start right on time, and must be "[speedy meetings](https://gmail.googleblog.com/2011/06/change-google-calendars-default-meeting.html)." This means they end 5 minutes prior to a 30-minute slot or 10 minutes prior to an hour slot
1. All teammates should have the meeting notes open during the meeting and add questions or comments in the doc in real time. Done well, this is much more effective than using Zoom chats.
1. The bi-weekly team meeting is a ritual that focuses on community-building not efficiency, and operates under a different approach to setting and managing agendas.


### Note taking during meetings
Taking notes during meetings provides structure to the call itself, but also makes it possible for teammates who are not available (or in a different timezone) to follow what happened and stay aware. Those who are not in the meeting should have access to the information, but not the obligation to review the notes unless explicitly informed that part of the discussion was relevant to them.

1. Everyone has the agenda doc open. Agendas can be "rolling" or "by date"
1. Discussion flows linearly through the doc -- from top down if organized "by date" and from bottom up if considered "rolling"
1. Add your name in the agenda as a way to signal that you'd like to speak. This helps prevent talking over each other and overcomes the slight delay that is experienced with videoconference.
1. Each teammate adds their point or question in real time during the call. Always add your point below others' that that the linear flow is maintained
1. Use numbers instead of bullets so they are more easily referenced in conversation
1. Each person's point should be its own bullet, so that you don't type on the same line as a teammate

NOTE: The general rule is that Google Doc agendas are for the real-time collaboration, and a general scratchpad attached to a meeting. It is expected that for every task that needs to be done after the meeting ends (the task that was discussed in the call), gets its own issue. This means that the person responsible for the task, is responsible for creating an issue and copying the issue back into the google doc. Once the issue exists, the rest of the discussion continues in the issue and not in the Google doc. The only exception here is if the issue contains items that need to be discussed again, at which point a question is asked in the agenda, with the link to the existing issue.


#### Using "rolling" vs. "by date" agendas

Rolling agendas are used for 1:1s with the CEO, and are suggested for most 1:1 interactions. They are valuable to ensure completion of topics (because those topics are removed as soon as they are done), and don't lend themselves well to sharing context with others.

"By date" agendas are organized as notes per meeting, and have a persistent record that can be shared with other participants. These are well-suited to meetings that may benefit from asynchronous participation or where notes may be referenced by people who were not present for the conversation. They work well for team meetings. These notes still should be considered "ephemeral" and action items moved to GitLab issues. See [GitLab usage](/people-group/gitlab/#using-gitlab-gdocs-and-handbook).

### Rolling agenda

Some of our meetings (and all CEO 1:1s) have a "Rolling Agenda." This means that the document is formatted as a numbered list, with each number representing a topic to discuss. New items are added to the bottom of the document, and in each discussion we work from the bottom and proceed as far up the list as we can. Once an item requires no more discussion it will be deleted. If we don't make it to a topic in one conversation it remains there for the next one, or until needed.

### Labels for agenda topics

It's recommended to prefix an agenda item with the name of the person who added it and a label indicating its purpose. Here are the labels and their meanings:

- `DISCUSS` - a topic that needs some discussion, explanation, or resolution.
- `DONE` - a topic that was previously marked `TODO` has been resolved and can be cleared by the person who originated the topic. Also used for a decision that has been made that requires no further discussion or communication. (Use TODO for decisions that need to be communicated to another party.)
- `FYI` - a "broadcast message" that you want to vocalize but don't expect needs much time. Expected response is either a clarification question or "got it".
- `HELP` - share a problem to solicit input on ways to approach. No need for resolution, and usually ends with "thanks for the input; I'll follow up shortly with a recommendation"
- `ISO_DATE` - (e.g. "2021-01-20") a way to postpone discussion of a topic until a particular date
- `MOVE` - conversation about the topic has been moved to a GitLab issue or epic
- `THANKS` - call out for great work or appreciation
- `TODO` - this requires action from one person. May also be used to note decisions that have been made that require communication to another party.
    - In a 1:1 context, TODO will be assumed to be the direct report, instead of the manager. i.e. it is shorthand for '==> Report'
    - In a 1:1 context `DOTO` can be used as short hand for '==> Manager'
    - The top-level item should be marked TODO and one of its sub-bullets should be prefixed with '==> Person' to indicate who has the action item
- `WONT` - to indicate that action was planned, but later decided not to be done. Topic should be cleared by its originator.


### Removing old items from a rolling agenda

The intent of the rolling agenda is for it always to reflect the topics that need to be discussed, or discussed again. This may be a topic that has gotten put on the back burner, or one that had been agreed to have a followup (a TODO) and needs confirmation from the person who requested it.

In order to keep the list only to active topics and prevent it from getting overwhelming, it's important to remove topics that no longer need discussion. It's actively discouraged to use the rolling agenda from retaining notes of what's been discussed. Notes of anything that's important should either be added to the handbook or moved into a GitLab issue. This ensures that their context is available to others, and at the time they're ready to address the issue.

Here's how to approach cleaning the agenda document:

- Each topic should be preceded by the name of the person who added it. This is the person responsible for removing the item from the list
- If you added a topic and don't need to discuss it further, just delete it. This is common for `FYI` or `THANKS`.
- If a topic had previously been marked with `DISCUSS`, `TODO`, or `DOTO`, it should first be marked as `DONE` or `MOVE` or `WONT` to communicate to the other person what its status is. This gives them an opportunity to confirm that they agree no further action is needed. Once discussed in the next agenda (or viewed asynchronously), the item can be removed. Remember: only the person whose name precedes the item can remove it.

## New Teammate Welcome Call

When a new teammate joins, we take the opportunity for the whole company to come together and welcome them to the team. Like the All Team call, this call is different than all other DoubleGDP meetings because it is mostly focused on the team  we're building, not our product or programs or customer relationships. We do this ritual to celebrate the growth of our team, to reflect back on how we've grown over time and how every single new person has changed the trajectory of our company, to form new relationships among ourselves and to help everyone know the newest member of the DoubleGDP team.


* We use our [all-team notes document](https://docs.google.com/document/d/19e9cAvzQUjctZydByialvsT-ufKH8nMgfztVqSS3Yw0/edit#) to guide the conversation
* We go through in order of Start Date from our [Team Milestones](https://docs.google.com/spreadsheets/d/1POhDzesk5fEjiafkWxJmsFnjbY5F_c--0fmFdSiO2vE/edit#gid=0)
* Each person introduces themselves, explains their role, and shares some tidbit about themselves (which changes for each meeting).
* Each person then calls on the next teammate on the list.


## Pre-mortem
A "pre-mortem" is a structured brainstorming technique to clarify our thinking around an important and time-sensitive effort, for instance a deployment of new software during a site visit. The technique encourages us to imagine that we've completed the project unsuccessfully, and use that hypothetical scenario to find improvements in the plan.

The process is:

1. Document a reasonable plan to deploy the software and share with the team ahead of time

1. During meeting (or in advance) imagine that we've finished deploying the software according to the plan and since have discovered that we did not achieve the desired results.

1. Describe the scenario that comes to mind, with specifics -- e.g. In what way(s) did we fall short? What's the situation? What has been communicated? What are the metrics we're seeing?

1. Work backward from that scenario to hypothesize some of the early manifestations or signs that pre-dated the failure -- e.g. in (hypothetical) retrospect, what were the early signs? Did we have a "bad feeling" or was it a surprise? When did we (or could we) have realized it wasn't working?

1. Now, consider some of the strategies that could have averted that scenario -- e.g. was this failure avoidable? What could we have done differently that might have worked better? At what point in the process should we have taken a different action?

1. Articulate some of those strategies and see if some are reasonable to incorporate in to the current best plan


Notes from a previous software deployment are available in our [Gate Access Deployment Pre-Mortem](https://docs.google.com/document/d/1aLZZte4eE69zjvNN6i0q0_Oz71wmMkN1z-Po4MrzTTQ/edit)


## "All Hands" Meetings

Every 6 weeks have an all-hands meeting where CEO shares an update on company strategy, progress toward goals, and ways we can contribute in the next period. Updates are provided in advance via Loom and the discussion is geared toward Q&A. Here are [slides](https://docs.google.com/presentation/d/12lI2G3zNv5dODfy4QwaFHcF4AJst0EsMTDolGSrFvq0/edit#slide=id.g1200b66a03e_0_84) and previous recordings:

- [2022-06-23](https://www.loom.com/share/cb786b69918a433cb80fb7587fc14ea1)
