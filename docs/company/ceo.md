---
title: CEO Readme
---

## Intro

The purpose of this handbook page is to document CEO processes and background that is helpful to teammates or external partners working directly with the CEO.

## CEO Staff / Strategy Team Meeting
The CEO hosts a weekly staff meeting of 75 minutes with all of his direct reports. This meeting has two primary objectives:

1. [Tactical] Help all functions of the company to stay coordinated around our efforts to hit our goals. Share progress, challenges, ask questions, explore creative approaches that may come from cross-disciplinary perspectives.
1. [Strategic] Discuss themes or issues around the broader context in which we operate and the impact we aim to have on the world. This is both to stay abreast of issues that impact our strategy, and to build the teamwork communication practices that will help us make effective strategic decisions quickly when they’re needed.

**Timing**. In order to use our time effectively and help build effective asynchronous practices, we ask some preparation in advance of the meeting:

- [Prep Form](https://forms.gle/v5d5vfaSgMbtP4b67) is due each Sunday by 5pm PT.

**Default Agenda**. Roughly speaking, we spend about 45 minutes each week on tactics and 30 on strategic. This is not strict, but a template that provides a default expectation for the meeting. This breaks down as follows:

Tactical [45 mins] -- Agenda to be driven by staff updates or questions from the tactical updates the day prior to the meeting

- [10 mins] Team connection. ~2-minutes each to share and connect on personal updates
- [10 mins] Construction pipeline. Head of CS to share progress on core goal (&56), challenges, and opportunities for help.
- [25 mins] QA on pre-meeting asynchronous updates, FYIs (ad hoc)

Strategic [30 mins] -- Agenda to be driven by CEO, with input or requests from team the week prior to the meeting

- 1-2 strategic topics teed up by CEO (preferably with pre-thought done by team. I.e. goal is to share topics a week in advance, with some reading material and even discussion done in advance. Hopefully this will be with nominations from team -- i.e. topics that people are wanting to discuss in more depth.)

**Getting on agenda**
- To request time during the meeting, add it to the agenda document the day before. Requests of up to 10 minutes are auto-approved (i.e. just add them); requests over 10 minutes should be discussed with CEO in advance.
- Label your agenda item with time requested (e.g. "[10]" minutes) and a label for what type of discussion you'd like to have. For example, `FYI`, `EXPLAIN`, `HELP`, `RESOLVE`
- During the meeting, the person who added the agenda item has the floor; they are responsible for returning the floor back to CEO at the end of allotted time.
- EA plays "time cop" during meeting, giving a warning when time on a topic has expired. We allow two minutes grace-period and then move on.

## Radar
A few notes on how the CEO keeps track of his radar:

- The [COMPANY GOAL epic](https://gitlab.com/groups/doublegdp/-/epics/68) collects the most important goals and initiatives for the company.
- The ["CEO Board](https://gitlab.com/groups/doublegdp/-/boards/2736407?scope=all&label_name[]=ceo-interest) tracks projects by Sprint milestones.
- The `ceo-interest` label is applied to other issues and epics in GitLab that are being driven by someone else but have particular interest to the CEO.

## Routine Processes

### 1:1 Meetings

CEO has weekly 1:1 meetings with each of his direct reports. They use the [rolling agenda](../meetings/#rolling-agenda) format. Each 1:1 starts with a < 10 minute recap from the direct report of "FYIs and Concerns". The purpose of this is to efficiently bring the CEO up to speed on the most important context for the rest of the discussion. They also serve as a practice for efficient and succinct communication and as a means to verify that the direct report is on top of the right set of issues. Each topic in the "FYIs and concerns" should be a sentence or two, and may entail a quick clarification or question. Items that need more discussion than that should be called out as separate agenda topics.

Each 1:1 also will touch on each GitLab issue assigned to the direct report that has the `ceo-interest` label.

### CEO Administrivia
This section is for CEO + EA to organize administrative tasks that need coverage:

1. Weekly on Fridays -- look ahead at next week's calendar and the [CEO Sprint Board](https://gitlab.com/groups/doublegdp/-/boards/2736407?&label_name[]=ceo-interest)
1. Monthly - review "Month End Close" financials from accounting
1. Daily - 25-minute CEO / EA sync to handle operations
1. Monthly - EA to share Slack metrics with team


### CEO Sprint Updates
At the end of each sprint, the CEO will publish and share an update from our company [Twitter account]((https://twitter.com/2xgdp)) using the ["CEO Sprint Progress Update" deck.](https://docs.google.com/presentation/d/1wrCcckL8qgNKSjWaHtbj0KS-F0A77YCuxJf5-0mwVdI/edit) It will be consistent with the process outlined in the [Sprint Demo](/engineering/team-works/sprints/#sprint-demo) and promoted according to our [social media guidelines](/marketing/social-media/#sprint-updates)

To create the update, here is the process followed. This may be a useful reference if someone is substituting for an update where the CEO is out.

1. Full team shares their sprint updates and adds to playlist
1. Watch the team playlist. Add reactions to [CEO Sprint Progress Updates](https://docs.google.com/document/d/18bBA8N4o3pOw1vpaTHkB7yE1xhI8paLDNB413O8DTCw/edit). Take screenshots of updates to highlight in CEO presentation.
1. Update the CEO Presentation linked above.
    1. Copy slides from last sprint. Update date on title slide
    1. Make the "Next Sprint" slide "This Sprint" and move to front of deck
    1. Delete unneeded slides
    1. Update the primary metric slide from [City Population Sheet](https://docs.google.com/spreadsheets/d/1IO5PP7vQSLWUuLZqQ_WY4tN_WgvmyxyntH3DjJbG9oM/edit#gid=816559689)
    1. Create one slide for each goal listed on the "This Sprint" slide and share the screenshots captured from watching playlists.
    1. Create a new "Next Sprint" slide that covers what we aim to do next, both in CS and Eng. Add a new graphic downloaded from [IconFinder](https://www.iconfinder.com/), if you want. ([Credentials in 1P](https://handbook.doublegdp.com/company/ceo/#sprint-updates).)
1. Record video (in Loom). Cover what we aimed to accomplish, how we did against those goals, and what we aim to do in next sprint.
1. Publish. Upload to YouTube; move to top of playlist; select the best image for the playlist thumbnail; tweet from the @2xdgp account, including a link to the playlist and the hashtags #remotework and #newcities.
1. EA follows up and re-posts from other social channels


### Virtual coffee talks
To keep a personal connection with how people are doing, the CEO holds virtual coffee talks with each person in the company. The aim is to do these at least once per quarter, and more often as time permits. These do not have a formal agenda, but you are encouraged to share your thoughts about what's working well and what could be improved culturally. We'll use the 1:1 Agenda document to collect thoughts if there are specific topics. Preparation is encouraged but not required.

## Reference Material

### CEO Biography

Sometimes it's necessary to share a CEO biography for a sales or partner getting to know us. The latest one is available in our [Shared Drive](https://docs.google.com/document/d/14238YHRkOUYO5TI_tgAi2WKeAIV_oSFUM5IVNAjMHNw/edit).

## Working effectively with the CEO

Here are some tips that will help communicate effectively with CEO

### Meeting requests

If you would like to schedule a call with me please include the following information in your request:

* Subject of the meeting
* Must have/optional attendees
* Urgency/ Desired timeframe: in the next two days, in the next week or two, etc
* Duration: 25 mins, 50 mins, etc.
* Provide context:

1. Every meeting has Google Doc accessible to the direct report and myself only
1. The doc is filled by the report 1 working day before the meeting
1. Clear expected outcome of the meeting


### CEO uses "consider" to share suggestions

The CEO likes to provide input and feedback on many decisions. Some of that may be a direct request (e.g. "please cc me on the response"), but often the feedback is for your consideration, but does not need to be implemented. In these cases the CEO will preface the feedback with "Consider" or "Suggest" to indicate that it is not a direct request. (e.g. "Consider changing this wording").

When CEO uses this nomenclature, the requested action item is for you to give consideration to the suggestion. It's appreciated if you follow up but not required or expected. It is NOT required for you to take the suggestion. (e.g. "I considered and decided to keep it as is" is a perfectly acceptable response.)


## Pro Tip on time boxing your hours

I find it helpful to time box in factors of 3. i.e. If you want to take a break, take an hour. If you need more, schedule three hours not two. 3 is enough different that you can do something different; 2 doesn't relax you much more than 1, but it is twice as costly.

The same principle is true for smaller increments. It's often valuable to try something quick -- see how far you can get on a task if you give it only 20 minutes. If it's more than that, reserve time in increments of an hour, or 3 hours, or a full day (6 hours).
