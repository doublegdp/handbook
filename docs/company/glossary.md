---
title: Glossary
---

## Glossary of Common Terms

Here are some terms we use at DoubleGDP that may require definition

Client
:   A paying customer of our `customer`, i.e. a person who is buying or leasing a plot of land.

Customer
:   Also referred to as "Partner" -- a city administrative team who is leveraging the DoubleGDP platform to connect with their residents

MorazanCity App
:   The DoubleGDP Platform, as viewed by someone within the Ciudad Morazan community

Nkwashi App
:   The DoubleGDP Platform, as viewed by someone within the Nkwashi community

Partner
:   Also sometimes referred to as "Customer". See that entry for definition. We often use "partner" in the context of our sales pipeline to convey that we are seeking new cities who want to partner with us on the journey of building great software and cities rather than those who are strictly looking for a software platform.

Prospect
:   Generally refers to a *prospective client* of our customer, i.e. a person who is considering buying a plot of land in the city
:   It may also refer to a *prospective partner* of ours, i.e. a city administrative team who we are discussing partnering with to use our software

Resident
:   An home-owner, renter, city staff or participant in a residency program who spends the majority of their nights sleeping in the city in the period during which they are counted. DoubleGDP and its city partners can jointly agree on the definition of a resident that meets the city's goals. 

User
:   One who is using the DoubleGDP platform, who could be a client or prospect of our customer, or even someone on the administrative team
