---
title: KPIs
---

## EA KPIs

1. Handbook contributions per team member per sprint (target: 1)
* [Handbook Contributions](https://docs.google.com/spreadsheets/d/18R2Rq_-S11tP7Cui4qj1sGNaEzCYM2sFRdO6HZEpHxE/edit#gid=739240944) 

1. Percent of Slack messages in public channels (no target)
* [Slack Public vs Private](https://docs.google.com/spreadsheets/d/1l-OqouvNAIhkhVM9bMK2pig3FexrlLdkjK48twQUpTg/edit#gid=0)

## Revenue (Customer Success and Sales) KPIs

### Management KPI
1. Curate at least one sales or customer success training recommendation per month 
1. Provide monthly customer reports 

### CSM - Individual Contributor
1. Identify at least one new use-case in a community per CSM per quarter
1. Deliver at least one product training per community per month per CSM

### Sales Rep - Individual Contributor
1. Complete at least 5 sales activities per week
1. Progress at least one deal or lead from one pipeline stage per sprint 
1. Complete the [product interest form](https://docs.google.com/forms/d/e/1FAIpQLSdvxApTv4VK139_JV8Y2Hd7UB8PRKpRhpW2mhKMZayci78QxA/viewform) for prospects at discovery stage. 


## Product KPIs

1. Number of payments and invoices recorded (no target)
1. Number of gate accesses and ratio that are from QR-codes (no target)
1. Weekly active users and daily active administrators (no target)
1. Weekly news articles read (no target)
1. Messages exchanged (no target)
1. Feedback received (no target)
1. Time cards submitted (no target)

Product KPIs are regularly tracked in a [product operations dashboard](https://docs.google.com/spreadsheets/d/1tR8KoJ8JWTFGmlAETWxfNx9UiC9JrJkHAiOiikXnIvQ/edit#gid=627688641). 

## Engineering KPIs

See [Engineering KPIs](/engineering/engineeringkpis/)


## Finance KPIs

1. Invoices paid
1. Monthly closes complete


