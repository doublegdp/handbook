---
title: Site Visits
---

## Site Visits and Company Travel

While DoubleGDP is a remote company, at time team members may travel to visit partners and city sites for product testing, training, relationship building, and hands on experience. To maximize the benefits of the visit and share learnings of trip across the team, the following process is followed:

- Create a trip folder and share notes on trip goal and plans
- Upload daily pictures from the visits
- Share daily learnings from the visit using a loom video with team
- Setup regular calls with [all@doublegdp.com](maitto:all@doublegdp.com) as optional for team to ask questions
- Add product or program ideas from the trip to Gitlab using standard or emergency escalation process
