---
title: Training Materials
---

## Training Materials

We have a set of videos for training and onboarding here. Note that videos themselves are private because some of them have personal details shown in screenshots.


### Intro to using Google Calendar
<iframe width="560" height="315" src="https://www.youtube.com/embed/HAQWJm9INRU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### How to create a batch list of clients
<iframe width="560" height="315" src="https://www.youtube.com/embed/R75TdrBCTt4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### GitLab Avatar How To
<iframe width="560" height="315" src="https://www.youtube.com/embed/zFxy9cG5Zns" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Google Avatar How To
<iframe width="560" height="315" src="https://www.youtube.com/embed/7x_QWbAH6pk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Slack Avatar How To
<iframe width="560" height="315" src="https://www.youtube.com/embed/oqIJRAkEqgs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Product Overview 

Part 1 (Last updated 2021-11-19)
<iframe width="560" height="315" src="https://www.loom.com/embed/cb09ee3eb34144f19d06fd165cb8a1b0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Part 2 (Last updated 2021-11-19)
<iframe width="560" height="315" src="https://www.loom.com/embed/94ca6ba48125403399547cb77301f595" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


Have questions? Please email [cecilia@doublegdp.com](mailto:cecilia@doublegdp.com) and ask!
