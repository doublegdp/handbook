---
title: Communications
---

## Primary Tools

Here are the primary tools that we use to collaborate with each other.

### Slack

Conventions for using Slack:

* Use public channels whenever possible. Threads within public channels can prevent spamming people, while still allowing others to be aware of what’s going on and to contribute if they have something to add.
* Posts will be considered FYI unless they mention a specific person @mention individuals to flag specifically for them
* Emoji acknowledgements are appreciated. See more on [GitLab’s page](https://about.gitlab.com/company/culture/all-remote/informal-communication/#using-emojis-to-convey-emotion).
* When communicating time in slack, prefer to specify in team's respective timezone or use UTC
* Use @channel for announcements
* Use MatterBot to give feedback and Kudos
* People can "browse all channels" and choose which channel to join, all internal DoubleGDP channels are open to employees. If you would like to rename or change the name of a channel please contact your Slack Administrator
* Slack channels, even private ones, should never be used to discuss specific personnel issues, concerns, or performance.  Membership in Slack channels changes over time and new members also see history; messages posted in channels therefore may at some point be visible to more people than who are included in the channel when it is written. (Personnel issues should properly be addressed in Slack DMs, email, Google Docs that are labeled with `[restricted]` and shared only with specific people, or in our Applicant Tracking System for candidates.)
* When responding in a public channel start a thread using the dialog above and to the right of the initial message. This will keep the conversation available for all to see but make fewer “unread” messages for teammates that are logging in after not checking Slack for a few days. Look for this icon:

<img alt="slack thread" src="../../img/slack_thread.png" width=200px />

* We can create "external" channels to improve communication with clients or external collaborators. The channel will be limited to collaborators with an invitation sent by email by our Slack administrator but open for all DoubleGDP employees once the group is created.  A list of participants should be provided and the main purpose of the channel should be specified.

* **TIP** You can create email notifications to avoid missing slack message.

<img alt="Slack Preferences" src="/img/communications/slack-notifications.png" width="600"/>

* Note that DoubleGDP has a goal to have 80% of communication in public channels. Current metrics can be view here: [Public vs Private](https://docs.google.com/spreadsheets/d/1l-OqouvNAIhkhVM9bMK2pig3FexrlLdkjK48twQUpTg/edit#gid=0)

### GitLab

This is our main project management tool. See our [GitLab page](/people-group/gitlab/) for more on how to use it.


### Google Suite

 Docs, Drive, Calendar, Spreadsheets

### Zoom

 For videoconferencing


## Communication Norms

Remote communication can be difficult at times, especially spanning timezones, cultures, work-styles, a variety of applications, and sometimes challenging connectivity issues. The following best practices are adapted from GitLab’s [communication guidelines](https://about.gitlab.com/handbook/communication/) and meant to help us maximize our asynchronous productivity. Some are general and some application-specific.

### General

1. Always acknowledge receipt of messages sent directly to you or in which you are mentioned. In email, send a “thank you” or “okay” or “got it;” in calendar respond accept or reject (or even maybe) to invitations; in Slack or GitLab a thumbs up. Acknowledging lets the sender know the message has been received and reduces the risk of “oh I was unaware.” It also alerts the sender that if they don’t receive a response it might have been overlooked and they should check in with you.
1. Favor written over verbal communication for decisions. If you work through something verbally, write it down so it can be shared with others.
1. Use roles instead of names when writing in the handbook. This is primarily because good processes are described by the actions required and the role of the person playing that action. Individuals play certain roles on certain projects, but may shift and change quickly even while the process stays static.
1. Timezone should always be specified. We operate in many time zones (e.g. PT, CT, WAT, CAT, EAT, HKT, IST), so this is important to avoid ambiguity.
1. Use “DoubleGDP” except in urls and email addresses, where it’s “doublegdp”.
1. In order to not miss a notification or meeting, you can download the Slack and Google Calendar mobile apps. It is not a requirement, but a nice to have.
1. Before sending a Google Calendar invite, please ensure that a link to a document is included instead of a file attachment. Adding an attachment doesn't allow users on iOS Calendar to click into the document. Links work on both Google Calendar and iOS.

### Sharing bad news

Share “bad news” quickly and straightforwardly. Be thankful to receive it -- bad news is so much harder to share than good news. We’re working on really ambitious and challenging projects; things will go wrong, and sharing them quickly and openly can usually provide good fodder for improvement for the whole company if handled with respect.

One thing that can make bad news so difficult to share is that it may come with a sense of self-recrimination. In this case it can be helpful to acknowledge the feeling first (e.g. "I'm concerned that I might have made a mistake") and then to forge ahead and share the news. We all make mistakes; it's easiest to help recover and learn from them if they're acknowledged straightforwardly.

Bad news also may be difficult to share if it comes with a sense of blame, e.g. if you believe someone else made a mistake. In this case, you should consider the mistake a separate issue from the bad news and consider sharing it as "negative feedback." [Give negative feedback in the smallest setting possible](https://about.gitlab.com/handbook/values/#negative-feedback-is-1-1) -- e.g. a 1:1 video call. Treating the bad news and negative feedback as distinct issues allows you to consider the timing and format of sharing each independently.


### Date format: yyyy-mm-dd

Dates should be written in [ISO format](https://en.wikipedia.org/wiki/ISO_8601): yyyy-mm-dd. This is unambiguous, machine readable, and sortable. This is especially important because we work in multiple countries; some default to M/D and some to D/M, so "5/4" could be interpreted as "April 5" or "May 4."

While it's recommended to specify yyyy-mm-dd for important communications, it is acceptable to use mm-dd as a shorthand. It is not acceptable to use slash ("/") or m-d. For example, it is okay to use "05-04" for "May 4", but not okay to use "5-4", "4-5", "5/4", or "4/5". Better still is to use the year, or the ISO recommendation of "--". For example: "2021-05-04" or "--05-04".

It is also acceptable to spell out the month or its abbreviation, e.g. "Jul 4" or "10-July" since these are also unambiguous. However, note that they are not machine sortable, and thus not as good as yyyy-mm-dd.


### Google Drive

#### Visible to all by default
Documents by default are visible to everyone in the company who has the link. You may disable this if you want to keep a document private

#### When sharing, give edit access to all
If you do share the document with anyone, share it with <a href="mailto:all@doublegdp.com">all@doublegdp.com</a>, but communicate directly with any individuals from whom you need input. This allows everyone to contribute and saves a lot of time when one stakeholder decides to add another, who may then need to request access from someone who has already stopped work for the day.

When sharing, uncheck the `Notify people` checkbox. It's not recommended to use Google Drive to notify, since you will share with all@. Share with everyone, but notify only those who you want to act upon it.

Conversely, documents shared to all@ should be considered “FYI” unless someone follows up directly with you. There’s no obligation to read every document that’s shared.

####  Be cognizant of PII
If you do share a document that includes Personally Identifying Information (PII) from one our partners share it with the specific group (Ex: data-partner@doublegdp.com) instead of  <a href="mailto:all@doublegdp.com">all@doublegdp.com</a>, this will follow our Not Public and Limited Access policy.


### YouTube

We post our sprint meetings on YouTube. We value transparency about what we’re working on and you’re welcome to share strengths and weaknesses, works in progress, highlights or concerns. However, we must also respect that not every external partner will share this level of comfort. Therefore we must be sure to avoid any of the following, in both our verbal narrative and any screens we share in presentations:

1. Personally identifying information who are not DoubleGDP team members. E.g. no mention of names, license plates, phone numbers, emails.
1. Statistics or metrics of our partners’ data

### Docs

1. Use numbered lists instead of bullets. They allow for being able to reference easily in conversation.


## **Not Public & Limited Access**

We make things public by default because[ transparency is one of our values](https://handbook.doublegdp.com/company/#transparency). Some things can't be made public and are either internal to the company or have limited access even within the company. The items that are not public are detailed in the sections below, together with an explanation of why they aren't public. If something isn't listed in the sections below, we should make it available externally.


### DoubleGDP Only (dgdp-only)

Some things are internal, available internally but not externally. These should be labeled with `[dgdp-only]` in Google Docs or `confidential` in GitLab. The following items are considered internal:

1. Financial information, including revenue and costs for the company, is confidential.
1. Deals with external parties like contracts, vendors, or partners.
1. Pricing on deals. Most pricing is covered in our [price list](/product/01-product-overview/#pricing-features); however if exceptions have been made in specific circumstances (or for Enterprise pricing), that information is considered dgdp-only.
1. Content that would violate confidentiality for a DoubleGDP team member, partner, or user.
1. Legal discussions are not public due to the purpose of Attorney-Client Privilege
1. Partner's information is not public since Partners are not comfortable with that, and it would make it easier for competitors to approach our Partners. Suppose an issue needs to contain _any_ specific information about a Partner, including but not limited to the company name, employee names, the number of users. In that case, the issue should be made confidential. Try to avoid putting Partner's information in an issue by describing them instead of naming them and linking to their Google Drive Folder. We can only discuss a Partner by name publicly if the partner is listed as "Do have permission" on our [partner confidentiality preferences list](https://docs.google.com/document/d/1PJ2aVAiZLh0yxTfS395AK2wtoEvlXGBuh9sd56LEQug/edit?usp=sharing). When we discuss a competitor (for example, in a sales call), this can be public as our competitive advantages are public.
1. If public information compromises one or more team members' physical safety, it will be made not public because creating a safe, inclusive environment for team members is essential to how we work.
1. Compensation Changes: DoubleGDP will communicate and train team members on the output of iterations (Compensation, Benefits), but team members will not have visibility into the inputs and decision-making of compensation changes.
1. Security vulnerabilities that we're aware of should be kept to DoubleGDP-only. More sensitive ones may be further restricted to "Limited Access."


### Restricted Access

The items below are not shared with all team members. These should be labeled with `[restricted]` in Google Docs and should *not* be included in GitLab. Restricted access is a more severe restriction than dgdp-only.

1. Deals with external parties like contracts and vendors.
2. Content that would violate confidentiality for a DoubleGDP team-member, Partner, or user.
3. Partner's lists and other Partner information are not public since many Partners are not comfortable with that, and it would make it easier for competitors to approach our Partners. If an issue needs to contain _any_ specific information about a Partner, including but not limited to the company name, employee names, and the number of users, the issue should be made confidential. Avoid putting Partner information in an issue by describing them instead of naming them and by linking to their Google Drive folder.
4. Plans for reorganizations. Reorganizations cause disruption, and the plans tend to change a lot before being finalized, so being public about them prolongs the trouble. We will keep relevant team members informed whenever possible.
5. Planned pricing changes. Much like reorganizations, plans around pricing changes are subject to shift management time before being finalized. Thus, pricing changes are limited access while in development. Team members will be consulted before any pricing changes are rolled out.
6. Legal discussions are restricted to the purpose of Attorney-Client Privilege.
7. Some information is kept confidential by the People Group to protect the privacy, safety, and security of team members and applicants, including job applications, background check reports, reference checks, compensation, terminations details, demographic information (age and date of birth, family or marital status, national identification such as passport details or tax ID, required accommodations), home address. Whistleblower identity is likewise confidential. Performance improvement plans, disciplinary actions, and individual feedback are restricted as they may contain negative feedback between you and your manager.
8. Acquisition offers for us are not public since informing people of an acquisition that might not happen can be very disruptive
9. Acquisition offers we give are not public since the organization being acquired frequently prefers to have them stay private.

Our default is transparency, but there is sensitive information that can either not be shared publicly or can only be shared with a limited internal group. We follow Gitlab's guidelines for [not public](https://about.gitlab.com/handbook/communication/#not-public) and [limited access](https://about.gitlab.com/handbook/communication/#limited-access) information.
