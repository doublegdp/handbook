---
title: CS Sprint Management
---

## Customer Success Sprint Management

The CS Team publishes sprint updates to inform teammates and customers stay informed about what we're working on, and invite them to engage further in topics that they are interested in.

The core sprint reporting topics:

1. Product Adoption - Status of product adoption efforts; User Guide edits; User trainings conducted; App feature requests and bugs raised
1. Sales & Leads - Update on the conversations with prospects and lessons learned that are beneficial to the DoubleGDP team


### Using Gitlab for Sprint Management

The CS team uses Gitlab tickets extensively to manage its work. Tickets created for CS work will include:

1. Milestone: The group milestone such as "Group Sprint xx mm-dd - mm-dd" will be used for all tickets. Where the delivery date is unknown the CS will default to the next available milestone. Where the work detailed in the ticket is likely to run longer than the sprint cycle, the CS team member will endeavour to create tickets in "sprint-size" chunks.

1. Label "cs-ops": This will be used for all tickets created by CS team.

1. Labels "housing stock"/"population"/"product adoption": These will be used where the ticket details fall into one of these categories

1. The Head of CS will use the Handbook User Board: CS Sprint Milestones that will have the label "cs-ops" and the respective milestone for planning and tracking sprint progress.


### Sprint Reviews

Sprint Planning and Retrospective Review is done every 2nd Monday during the Customer Success Team sync.

Sprint Retrospective topics include:


1. What was achieved in the previous sprint?
1. What will move to the next sprint or will progress over several sprints?
1. Lessons Learned/ what could be done better

Sprint Planning topics include:


1. What is planned for the upcoming sprint?
1. Help required within the sprint


### Documenting the Sprint Update

Slides for the updates are available through GDrive as "[CSM Sprint Update](https://docs.google.com/presentation/d/1Yq1kPRROuXTLs3ZKLNvL8OcPGjGJdzkeS_9Gn44yrwk/edit#slide=id.g9aa6f6f5d2_0_201)"

Each topic mentioned in sprint updates will be linked to a corresponding issue in GitLab. This has a few purposes:

1. Depth of information. We provide summaries in video talking points, but the link provides a reference where the audience can learn more.
1. Engagement. We encourage participation and the link provides an easy entry point for teammates to provide input or help on specific issues that interest them.
1. Communication consistency. This helps us ensure that we're utilizing GitLab for all our processes and leveraging handbook-first evolution.

The sprint update recording is uploaded to [DoubleGDP's YouTube channel](https://www.youtube.com/channel/UCALY7l5iisNVrEyvLgQa3ig). The video should have the CSM's name suffixed and should be added to the sprints playlist: "Progress Updates yyyy-mm-dd".
