---
title: People Group Homepage
---





## **Time Zones**

We aim that most of our work should by "asynchronous" -- meaning that you can do it on your own schedule and not be dependent on meeting with another teammate in order to tackle it. However, coordination is also important both for work and social reasons. Since we span time zones, currently `US/Pacific` and `Africa/Lusaka`, we ask that team mates make themselves generally available on Slack and for meetings during those hours that are the best overlap between them:

| Overlap | US/Pacific | Africa/Lusaka | Honduras (CST) |
| --- | --- | --- | --- |
| Start Time | 7:00 am | 16:00 | 9:00 am |
| End Time | 10:30 am | 19:30 | 12:30 pm |


*Note that these are overlap times while the US observes Daylight Saving Time. We will adjust in November when this period is over.*

## **Security**

### In case of lost or stolen property
Employee should immediately notify [all@doublegdp.com](mailto:all@doublegdp.com). 
