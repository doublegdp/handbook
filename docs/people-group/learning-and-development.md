### Courses

DoubleGDP encourages team members to invest in their personal growth.

### Coursera

The company has an account with Coursera and any teammate may choose to join the company plan and take as many courses as they wish. This login is available in the OnePassword PeopleOps vault.

### LinkedIn Learning

DoubleGDP is piloting LinkedIn Learning as an additional online course provider. Individuals can subscribe to LinkedIn Learning to take advantage of the numerous training courses. During the pilot period, the subscription is renewable monthly and can be expensed accordingly.

Speak with your line manager before setting up your LinkedIn Learning Subscription. They will provide guidance on the course priorities for your team.

Once a course is completed, LinkedIn provides a certificate. A copy of this must be shared at the end of each course. This process will be used to track utilization and therefore confirm extension after the pilot. 

Note: you can expense your LinkedIn Premium Membership via Expensify.

### Other Learning Opportunities

Teammates may spend up to $600 USD per year on educational books, other courses, or professional certifications, so long as they are discussed with their manager in advance, they have the time available while fulfilling their work responsibilities, and they are relevant to their DGDP responsibilities.
