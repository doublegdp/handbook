# Workstation setup

You need to have a good workstation to work from home to be a productive contributor at DoubleGDP. Full-time teammates may use company money to purchase [office equipment and supplies](https://about.gitlab.com/handbook/spending-company-money/#office-equipment-and-supplies), as they do at GitLab. (Part time teammates or contractors should ask their manager before making purchases.) We mostly follow the guidelines described in their policies, though one important distinction is that within DoubleGDP, purchases exceeding $500 USD are company property and required to be tracked in the [Employee Equipment - Fixed Asset Tracking sheet](https://docs.google.com/spreadsheets/d/1QCesUFmXBiuTcgf2UI5jnc7M4QjqwpcoMhw8rXaGgGs/edit?usp=sharing). There's a link below that provides details on reasonable prices for items on the list, and look at our [finance page](https://handbook.doublegdp.com/finance/) for instructions on how to file expenses.

### Required setup

The following setup is required for all teammates:

- Computer -- see [laptop purchase and setup](https://about.gitlab.com/handbook/business-ops/it-ops-team/#laptops) from GitLab. We recommend [Apple MacBook Pro 14", 512 GB](https://www.apple.com/shop/buy-mac/macbook-pro/14-inch-space-gray-8-core-cpu-14-core-gpu-512gb#) for non-engineers and [Apple MacBook Pro 16"](https://www.apple.com/shop/buy-mac/macbook-pro/16-inch-space-gray-10-core-cpu-32-core-gpu-1tb#) for engineers.  Team members may refresh their laptop every 36 months.
    - Please submit your Apple purchase request to [PeopleOps@doublegdp.com](mailto:PeopleOps@doublegdp.com) ***If you're new to Macbook, please visit our [Knowledge Management](https://handbook.doublegdp.com/people-group/knowledge-management/) page in the handbook to learn the new operating system***.
    - We recommend US team members purchase Apple Care when procuring a new laptop. This should streamline repairs and support if issues arise that need IT help. (Non-US should purchase if it is relevant in their locality.)
- [Camera](https://www.logitech.com/en-us/products/webcams/c920s-pro-hd-webcam.960-001257.html?crid=1865) for videoconferences
- [Light](https://www.amazon.com/Webcam-Light-Stream-Selfie-Logitech/dp/B07G379ZBH/ref=sr_1_11?dchild=1&keywords=desk+key+light&qid=1609263224&refinements=p_85%3A2470955011&rnid=2470954011&rps=1&sr=8-11) that makes your face visible on videoconference calls
- High-speed, reliable internet
- Power source, if it's not reliable in your area. We recommend solar power as it is efficient.
- Quality [headset](https://www.bestbuy.com/site/logitech-h390-usb-headset-with-noise-canceling-microphone-black/9618932.p?skuId=9618932) with microphone for videoconferences **or** [external mic](https://www.bluemic.com/en-us/products/snowball/) and [headphones](https://www.apple.com/shop/product/MYMD2LL/A/beats-flex-all-day-wireless-earphones-yuzu-yellow?fnode=b4a25f917af76a6a61134216a970e439aaae4b86fdbd20c110280fd0c15ec958071c38fb99ee36b56abb802ffc283f8a0993ec8b242208d7b53ef10758d2fbefb459bcd5ce1a4033548a5add254353db58e62425761af9065cd47c23f4aa4faba4bf87e06d83b261b6c7058a5e1c31e1) combo (up to personal preference). The headset price should not exceed $300.00. 

GitLab offers guidelines on [reasonable prices](https://about.gitlab.com/handbook/finance/expenses/#hardware) and recommendations on specific pieces of hardware.

### Recommended hardware

These items are highly encouraged:

- [LG Monitor](https://www.lg.com/us/monitors/lg-32UD60-B-4k-uhd-led-monitor)
- Upgraded webcam -- something better than what comes with your laptop, and that can be mounted on your monitor
- Comfortable, [ergonomic chair](https://www.amazon.com/Hbada-Ergonomic-Office-Chair-Adjustable/dp/B07P7MQ9D5/ref=sr_1_11?dchild=1&keywords=ergonomic+chair&qid=1609265667&refinements=p_85%3A2470955011&rnid=2470954011&rps=1&sr=8-11)
- [Adjustable height desk](https://www.autonomous.ai/standing-desks/smartdesk-2-home?option1=1&option2=1980&option16=38&option17=1881&utm_term=438971237506&utm_campaign=performance&utm_source=google&utm_medium=shopping&utm_content=&hsa_acc=9219256787&hsa_cam=10203432663&hsa_grp=100642041023&hsa_ad=438971237506&hsa_src=u&hsa_tgt=pla-293946777986&hsa_kw=&hsa_mt=&hsa_net=adwords&hsa_ver=3&gclid=CjwKCAiAxKv_BRBdEiwAyd40N11y62j-iUDWXGSOPmqEqUzN9q_PMlgKO_PM-FbPMtaSomNHnQ94mRoChd8QAvD_BwE), or a [standing desk converter](https://www.amazon.com/VIVO-Adjustable-Converter-Tabletop-DESK-V000V/dp/B0784HWPN6/ref=sr_1_5?dchild=1&keywords=standing+desk+converter&qid=1600277717&sr=8-5)


We recommend you consider these items:

- [Mouse](https://www.apple.com/shop/product/MLA02LL/A/magic-mouse-2-silver) and [keyboard](https://www.apple.com/shop/product/MLA22LL/A/magic-keyboard-us-english)
- Coworking space
- Office decorations that help you feel productive or look good on Zoom
- Wifi router
- Android phone (Equipment provided by the company only for CSMs)
- Internet connectivity
- Google Voice - US-based phone numbers are available to all employees if needed, or need to register a phone number with external parties but don’t want to share your personal line. Please request Gvoice account from People Ops via the `#peopleops` Slack channel.


Note that internet connectivity may be expensed if it is used primarily for business and local law permits us to pay for it without incurring taxes. We recommend you aim for at least 10Mbps, but that you make relevant cost / bandwidth tradeoffs based on your local area. Expenses should be submitted monthly using our [expense guidelines](https://handbook.doublegdp.com/finance/).

### Recommended Power backup
To avoid interruptions when there is no electricity, this is a setup we recommend using:  

- 100 Watts Solar panel   
- 500VA - 1000va Hybrid inverter  
- 100AH 12V Battery  

### Shipping hardware from the U.S. to Africa

We recommend using the following parcel forwarding services:
- [Heroshe](https://www.heroshe.com/): to ship goods from the U.S. to Nigeria.
- [iShop Worldwide](https://www.ishop-worldwide.com): to ship goods from U.S. or the U.K. to Zambia or Nigeria.

For both platforms, the process is as followed:

1. Sign up to the platform.
2. Order items to the warehouse address indicated on the respective websites.
3. Pay the shipping fees to platform.
4. Receive items in Nigeria or Zambia.

For warranty reasons, we recommend buying directly from the manufacturer rather than platforms such as Amazon.

### Email Signature & Logo

Please add our [company logo](https://drive.google.com/file/d/1rt9HbzOi2J2kCZbQPU7QO_vM6_BjatMP/view?usp=sharing) to your email signature.


### Mac OS tips

If you're new to Mac, please review this helpful [video](https://www.youtube.com/watch?v=67keaaWOKzE) intro to OSX from Windows.

Using the trackpad:

* A "right click" is "two finger tap" on the track pad. This doesn't apply if you're using a magic mouse.
* Swiping up on the trackpad with two fingers is scroll.
* Swiping up on the trackpad with three fingers allows you to see all open windows.
* “Two-finger push upward” makes the page scroll down.
* “Two-finger push downward” makes the page scroll up.
* Be aware of ["Hot Corners"](https://support.apple.com/en-mk/guide/mac-help/mchlp3000/11.0/mac/11.0) -- this can be startling if you're not used to it

Keyboard commands:

* Command + C = copy
* Command + V = paste
* Command + X = cut
* Command + Shift
* Command + space pulls up Spotlight which allows you to search files (eg documents and apps)
* Command + shift + 4 allows you to take a screenshot
* Change settings on Magic Mouse to support right click

Other recommended settings:

* Remove non-useful native Mac apps from your menu bar with right click on the App Icon.
* Allow Zoom to control your computer
* Use Chrome instead of Safari
* Use Gmail and Google Calendar in browser rather than Mac Mail
* Use Google docs over Numbers, Pages, and Keynote
