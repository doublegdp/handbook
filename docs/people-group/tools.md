# Productivity Tools

We use a variety of productivity tools in addition to those mentioned in [Communications](#communications). You’re welcome to use your own private productivity tools. Here’s a list of those that the company licenses for shared productivity or for which we have company accounts:

| **Application** | **Purpose** |
| --- | --- |
| [Airtable](https://airtable.com/) | Knowledge base of articles and company contacts |
| [Bill.com](https://www.bill.com/) | Manage invoices and accounts payable |
| [Brex](https://www.brex.com/) | Company credit card |
| Calendly | Calendar scheduling |
| Coursera | Learning and development |
| [CultureAmp](https://www.cultureamp.com/) | Performance Review Tool |
| Digital Ocean   | [engineering] Remote dev environment   |
| Docker   | [engineering] Containerization of packages for deployment |
| [Earth Class Mail](https://www.earthclassmail.com/) | Manage postal mail online
| [Expensify](https://www.expensify.com/)   |  Submit expenses to finance   |
| Facebook   | Social media: <a href="https://www.facebook.com/doublegdp/">https://www.facebook.com/doublegdp/</a>   |
| Figma   | User Interface Design: <a href="https://www.figma.com">https://www.figma.com</a>   |
| [Formspree](https://formspree.io/) | Contact us form on website (login in Shared 1Password vault) |
| Gitlab   | Source control and website hosting    |
| Google Analytics   | Website traffic analytics   |
| [Google Cloud](https://console.google.com) | Dev tools |
| [Google Data Studio](https://datastudio.google.com/) | Reporting |
| Google G Suite   |  Email and office applications   |
| [Google Marketing Platform](https://marketingplatform.google.com/home) | Linked org for Analytics and Data Studio |
| Google BigQuery | Data replication via Google Cloud |
| [Greenhouse](https://www.greenhouse.io/)   | Applicant Tracking System and job board   |
| [Gusto](https://gusto.com/)    |  Payroll for US-based teammates |
| Heroku   | [engineering] Cloud application hosting |
| Hexnode   | Runs our app in kiosk mode for security guards’ phones   |
| [Hootsuite](https://www.hootsuite.com/) | Posts to and monitors a variety of social media channels |
| Invision    | [still in use?]   |
| [LinkedIn](https://www.linkedin.com/)   | Social media: <a href="https://www.linkedin.com/company/doublegdp">https://www.linkedin.com/company/doublegdp</a>  |
| [Loom](https://loom.com) | Record and share videos easily |
| [Material-UI](https://material-ui.com) | React Javascript Library |
| MatterBot   |  Allows you to recognize team members with Kudos, constructive feedback, and rewards in Slack.
| Nexmo   | [engineering] Send SMS messages from app   |
| [1Password](https://1password.com/)   | Store shared passwords and account information    |
| PagerDuty  | On call schedule to receive app error / outage notifications. Trigger a notification through +1-802-990-0911 |
| [Phrase](https://phrase.com) | Application translation and localization |
| Pipedrive  |  Sales customer relationship management tool
| [QuickBooks](https://app.qbo.intuit.com/app/login?pagereq=paymentHistory&locale=en_US)   | [finance] Manage accounting and payments   |
| [Ramp](https://ramp.com/)   | Company Credit Card for CSM
| Redis   | [engineering] Data store, cache, and messaging   |
| Rollbar  | [engineering] Detect and manage app exceptions   |
| SendGrid | Email messaging support through the app |
| [Silicon Valley Bank](https://www.svb.com/) | [finance] Manage cash and make wire transfers   |
| [Slack](https://slack.com/)  | Team communications & chats   |
| [Stitch](https://www.stitchdata.com/) | Connect Heroku with BigQuery databases |
| Storybook  | React Documentation Component https://doublegdp.gitlab.io/app  |
| [Twitter](https://twitter.com/) | [Marketing] Handles: @nolanmyers and @2xgdp  |
| [Visual Studio Code](https://code.visualstudio.com/) | Engineering software development code editor |
| WhatsApp[^wa] | Commonly used to communicate with people internationally   |
| [Wise](https://wise.com/)  | International Payments App.  |
| [YouTube](https://www.youtube.com/)   | Community updates in [company channel](https://www.youtube.com/channel/UCALY7l5iisNVrEyvLgQa3ig?view_as=subscriber) |
| Zeplin | UI Design and style guides |
| [Zoom](https://zoom.us/)  | Video conference calls |


[^wa]: Not licensed, but commonly used for work-related activity
