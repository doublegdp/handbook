# Company Holidays


## **COMPANY HOLIDAY - Christmas Week Off**

DoubleGDP enjoys a company Holiday Week at the end of the year. We close our offices for the week so people can take this time to rest and celebrate the holidays, and focus on their loved ones.

Please remember to update [PTO by Roots](/people-group/paid-time-off#pto-by-roots) and select Christmas Holiday Week.

For 2022, this holiday will be observed 2022-12-26 (Mon) - 2023-01-01 (Sun). We resume operations on 2023-01-02 (Mon).

For Support during the holiday, please refer to the [2021 Holidays On Call Schedule](https://docs.google.com/spreadsheets/d/14zc5ARkvYHFQIEbPBtrZLm2oyyolP4cA-ks8Ie64Bnw/edit#gid=0)

### 2021 End of Year Celebration - *Time extended through 2022-01-31*

The end of year is a natural time for celebration and togetherness. For many companies, an "end of year party" is a time to be together with coworkers and to share the experience with significant others. We also want these qualities, but as an all-remote company, take a different approach.

We ask that each team member have a special celebratory dinner with their family or close friends, courtesy of DoubleGDP, and then share pictures and stories from the experience with us in the #celebrations Slack channel. We hope this will help each person enjoy the festive spirit with their loved ones, and for us all to get to know one another better through the stories.

Look for this invitation is Slack:

<a href="https://doublegdp.slack.com/archives/C01GR6W59B6/p1638979415010000"><img alt="End of Year Celebration Invitation" src="/img/people-group/end-of-year-celebration.jpeg" width="150" style="border: thin solid gray"/></a>



## **COMPANY HOLIDAY - Spend the day with your Family and Friends**

At DoubleGDP, we value family and friends. In an ongoing pandemic with COVID-19 affecting many of our team members, we want to ensure that people are making their well-being a top priority and that we are living out our values, to emphasize this we will coordinate a Quarterly "Family and Friends Day" for as long as the majority of the world, where our team members reside, are dealing with COVID-19. On this day, we will close the doors to the DoubleGDP virtual office, reschedule all meetings, and have a **publicly visible shutdown**.

Team members can share about their Family and Friends Day in the `#family-and-friends-day` Slack channel after the event, or publicly on social media such as Twitter, LinkedIn, or wherever they're most comfortable using the hashtag #FamilyFriends1st. Sharing is optional. Taking the day off is strongly encouraged if your role allows it.

### Upcoming Family and Friends Days

1. 2021-09-20
2. 2022-03-21
3. 2022-07-15
4. **2022-09-23**

We will look at scheduling future dates roughly quarterly. In line with our [Paid Time Off](/people-group/paid-time-off) policy, we encourage DoubleGDP Team Members to continue to take additional days off as needed. Family and Friends Day is a reminder to do this.


### FAQ about Family and Friends Day

### Who determines upcoming Family and Friends Days?
Any DoubleGDP team member is able to propose a Family and Friends Day. To propose a Family and Friends Day please follow the steps outlined below:

1. Review the DoubleGDP Team Meetings calendar for major conflicts, review major holidays, and avoid scheduled Investor calls and Customer Council Meetings.
1. Submit a request to peopleops@doublegdp.com

### I'm in a role which requires me to work that day. How can I still benefit from this initiative?
If you are in a role that requires you to work on Family and Friends Day, you can work with your manager to find an alternative day. We encourage you to consider the following business day as the preferred second choice for a day away, but do what works best for you and your team.

### What if the date is a public holiday or non-working day in my country? How does this apply to me?
We encourage you to take off the next working day. If this day isn't an option, work with your manager to find another day that works for you and your team.

### How is this any different than our vacation policy?
Nothing about our [Paid Time Off](/people-group/paid-time-off) policy is changing. We wanted to designate a specific day in order to more proactively force a pause for team members. If most of the company isn't working, there is less pressure for you to do so.

### What about client or prospect meetings that conflict?
If you feel that this meeting can be rescheduled without any setbacks to the business, please go ahead and do so. If you have a meeting that would be hard to reschedule or would jeopardize the business results, please work with your manager to find another day that would work for both you and your team.

### What if I'm out sick on either of those days?
Feel better! Please work with your manager to find another day that works for you and your team.

### How do I communicate that I'm off that day?
We'll assume that most people are off on Family and Friends Day, but we know that some people will take other days.

Please update [PTO by Roots](/people-group/paid-time-off#pto-by-roots) in Slack. You can select `Create an OOO Event` and find `Family and Friends Day` in the drop-down menu of `What type of OOO is this?`.

Feel free to block your calendar with "Family and Friends Day" to share whatever day you take.
