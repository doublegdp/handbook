# Recruiting

We aim to build a world-class team. This page goes through processes for internal teams and candidates to help support that goal.

## Jobs and boards

We use [Greenhouse](https://app3.greenhouse.io/) to host all jobs and manage our recruiting and interview process. All team members are welcome to participate, and will be asked to play different roles for different positions. Our open jobs are listed on [our website](https://www.doublegdp.com/about/).

If you are a Recruiting Agency please contact [peopleops](mailto:peopleops@doublegdp.com) to grant you access to [Greenhouse](https://app3.greenhouse.io/)
Please follow these steps:
 - Add candidate's profiles to [Greenhouse](https://app3.greenhouse.io/) with notes from the screening call.
 - Manage candidate's interview stages Eg: Screening Call, Panel Interview, CEO Interview, etc
 - Email recruiting manager for availability for interview.
 - Set up a weekly follow-up call with the recruiting manager.

## Internal Referral Incentive Program for Software Engineers
DoubleGDP is offering a referral bonus to team members who refer a software engineer that we decide to hire. If you know someone who you think would be a good fit for a position at our company, feel free to refer them. **If we end up hiring your referred candidate, you are eligible for $500 referral bonus.**
Additional rules for rewards:

1. There is no cap on the number of referrals an employee can make. All rewards will be paid accordingly.
2. If two or more employees refer the same candidate, only the first referrer will receive their referral rewards.
3. Referrers are still eligible for rewards even if a candidate is hired at a later time or gets hired for another position.
4. Reward will be paid 6 weeks after the candidate starts at the company and is in good standing


## Factors Looking For

At a high level, we value teammates with intelligence, integrity, and strong work ethic. These traits should be looked for during the interview process. In addition, there are specific factors relevant to all candidates applying to DoubleGDP:



1. **Alignment** - clear what candidate is looking for and that it aligns with what we need
2. **Domain experience** - SaaS software, new product launches, startup companies. Note: focus on stronger of resume / LI Profile, recognizing that different people put emphasis on different mediums.
3. **Tech expertise** - web technologies, agile stack
4. **Leadership** - have a player / coach mentality. Want to contribute by doing and by helping others.
5. **Communication** - concise, clear, and able to cite specifics. They write and communicate well, and avoid glaring errors.
    1. Note that we make affordances for challenges commonly faced by non-native speakers. For example, issues that spell check can catch should be a red flag, but more subtle grammatical or phrasing that may seem awkward to a native speaker should not.

Each job may have other specific requirements that will be kept in the job description or scorecards within Greenhouse.

## Interview Process
Below is an overview, context, and pointers to key steps in our interview process. A few notes on the global process:

- All team members who participate in the interview process should have Greenhouse accounts and be added as `Interviewer` to the role for which they are evaluating candidates.
- All feedback must be added as a `Scorecard` at the end. Reviewer should note observed strengths, any surfaced concerns, and (if appropriate) any open questions. Cite specifics in order to help with your memory and help the manager and other team members interpret your feedback.
- All Scorecards are summarized with a 4-point rubric, interpreted as follows:
    - `Definitely Not` - Based on this interaction there were serious cultural or behavioral red flags that were surfaced. Scorecard should articulate what those were. This could also be used to indicate that there was no evidence that the candidate had the necessary skillset; however, hopefully these situations are screened out prior to the candidate having an interview.
    - `No` - Based on this interaction, Reviewer would recommend not proceeding. Usually this indicates that the reviewer did not end with a sense of confidence that the candidate demonstrated the necessary skill. If the candidate did not convince you, this is the option to choose.
    - `Yes` - Reviewer felt confident that candidate can do the job and will be a good addition to the team. Candidate actively demonstrated skill and will to perform. Reviewer must cite specifics in order to use this; however, there may still be open questions or red flags that are communicated and can be followed up on by others.
    - `Strong Yes` - Reviewer was very impressed and has specific reason to believe that this candidate had the necessary skill and will to perform more than what's being asked in the role, and seemed exceptional relative to other candidates. Reviewer is excited about adding this candidate to the team.
- It is recommended that after each round and all scorecards are complete, all reviewers meet synchronously for a few minutes with the manager to provide color and context and Q&A about their responses.


### Internal Feedback
For positions reporting to CEO, candidates will speak with multiple team members. Each team member should enter their feedback into Greenhouse, without discussing it first with others. Then we will use 10-15 minutes at the next Strategy call to discuss the feedback synchronously.

### Engineering Interview Process
Open positions are posted on our [job board](https://doublegdp.com/jobs). Please apply through there, and your application will be reviewed and responded to. If it appears there's a good potential fit, here's an overview of the steps in our engineering interview process:

1. Initial introduction - 25 minutes with either CEO or Head of Engineering. What we'll go through:
     * High-level meeting call to ensure the candidate and DoubleGDP align on job requirements
     * What DoubleGDP and the position are about to ensure it matches your expectations
     * Resume walk through – We want to make sure we fully understand your work experience
     * What you are looking for in your ideal role?
     * What you should expect in the next steps of the hiring process
1. Coding challenge - 2-3 hour exercise to demonstrate your strengths
1. Technical interview - 50 minutes with Head of Engineering or appropriate technical resource
     * Use [this template](https://docs.google.com/document/d/11SZQBCEpkegel5slpf43d7WSa-kL_E-gqJtfOUzX6dg).  Make a copy and fill in the relevant information.
1. [CEO interview](#ceo-interview) - 50 minutes with CEO
1. Interview panel - Optional - 40 minutes with a representative of each team (engineering, partners, customer success)
1. References -- 3-5 references including a manager, a peer, and cross-functional partner

### UI/UX Designer Interview Process
Open positions are posted on our [job board](https://doublegdp.com/jobs). Please apply through there, and your application will be reviewed and responded to. If it appears there's a good potential fit, here's an overview of the steps in our engineering interview process:

1. Initial introduction - 25 minutes with either the Product Manager or CEO. What we'll go through:
     * High-level meeting call to ensure the candidate and DoubleGDP align on job requirements
     * What DoubleGDP and the position are about to ensure it matches your expectations
     * Resume walk through – We want to make sure we fully understand your work experience
     * What you are looking for in your ideal role?
     * What you should expect in the next steps of the hiring process
1. UX team Interview - 40 minutes with a representative of the UX team
1. Competency based interview - 50 minutes with Product Manager or submission of the [UX Case Study challenge ](https://docs.google.com/document/d/12DjEmw5NMPLI5KPmeBOkjUhWZsSbxc7vhMJGeAk870Q/edit?usp=sharing)
1. Interview panel - 40 minutes with a representative of each team (engineering, partners, customer success)
1. [CEO interview](#ceo-interview) - 50 minutes with CEO
1. References -- 3-5 references including a manager, a peer, and cross-functional partner

### Customer Success Manager (CSM) Interview Process

Customer Success Managers are an integral part of the DoubleGDP team and a rigorous process is used in the selection process. See our [job posting](https://www.doublegdp.com/jobs/) for details on the position.

Interview questions template can be found [here](https://docs.google.com/document/d/18EC2BdUXvhzTnXiwL0ZNuo4w8YQ8dr5K-N7UxLf1WP8/edit#) (internal to DoubleGDP) while these are steps in the process:

1. Use the [CSM Interview Notes](https://docs.google.com/document/d/1oRk8SeOuqnB24wAOwsUXnkzscyCrq98qdLbwkrQBQWg/edit#heading=h.7zecvmj0myvd) document for all interviews notes.
1. Initial introduction - 25 minutes with Head of Customer Success. This is structured as:
    * High-level meeting call to ensure the candidate and DoubleGDP align on job requirements
    * Resume walk through to identify transferable experience
    * What you should expect in the next steps of the hiring process
1. Technical Interview/Case-Study Exercise -  50 minutes with Head of Customer Success where candidate’s technical knowledge of the CSM role will be assessed. The [case study details](https://docs.google.com/document/d/1sA53mOHTJYBy47w4Fnef7S8zDdxy12Nio_y3PMPysK0/edit) are provided to candidates that have been shortlisted to this stage.
1. Product/Engineering
    * Technical assessment for non-engineers
    * 50 minutes with the Head of Product/Head of Engineering or representative. The candidates application experience and knowledge will be assessed in this session
1. Peer-Panel Interview - 30 minutes with CSM to assess cultural fit
1. CEO interview - 50 minutes with CEO
1. Synchronous discussion between hiring manager and other interviewers to make a decision on the  candidates that have successfully passed the previous interview steps.
1. References -- 3-5 references including a manager, a peer, and cross-functional partner


### Head of People Interview Process
We are looking for a strategic leader to bring our product to the next level. See our [job posting](https://www.doublegdp.com/jobs/?gh_jid=4760046003) for details on the position. Here are the steps in the process:

1. Use the [Head of People Interview Notes](https://docs.google.com/document/d/1vuO9lHqAPP-Frh5q--sad0hTeE8uGAR9SYdUDumHY3Y/edit#heading=h.344366vfzmgn) document for all interviews notes.
1. Introductory conversation(s) with our recruiter. Get to know the company, CEO, and determine if there's a reasonable overall alignment of interest and experience. 25 minutes.
1. [Interview with CEO](#ceo-interview). Go in-depth to your background and experiences. 50 minutes.
1. Panel interviews. Meet some of your potential peers on the leadership team. Technical & Customer management. 50 minutes each.
1. Exercise. Demonstrate skills in building culture, performance management, and/or compensation planning. 1-2 hours prep + 50-minute meeting.
1. CEO Conversation (if necessary). If there are follow-up questions from the previous rounds or further topics to discuss, we'll allocate more time.
1. Investor Interview. This role requires meeting the DoubleGDP investor. See [investor interview tips](#investor-interview) below. 50-minutes.
1. Reference checks. We'll want to learn more about you from people you've worked closely with. See [references](#references) below


### CEO Interview

Before your CEO interview, please be sure to have watched our most recent [sprint update](https://www.youtube.com/channel/UCALY7l5iisNVrEyvLgQa3ig/playlists?view_as=subscriber). The CEO interview tends to be fast-paced and values succinct answers. There's a lot to get through in a short amount of time, and the best interviews allow for follow up questions on your initial responses. Here are the some of the questions that will likely be asked:

1. How would you introduce DoubleGDP to a new prospective customer, colleague, or friend?
1. Will you walk me through your resume?
1. How did you first become interested in the field of work? How have your interests changed over time, and what has influenced your choices?
1. What interested you in DoubleGDP? Have you been looking at other opportunities? If so, what do they have in common, and in what ways are they different?
1. What aspects of our mission excite you most? Are there areas that you see as potential concerns? What do you think will be some challenges we will face in the next 6-12 months and what are your thoughts on how to address them? (Please share some thoughts about challenges pertinent both to our company as a whole and to your role in particular.)
1. Tell me about a complex project that you worked on. What made it challenging? How did you approach it? How does it exemplify how you approach your work?
1. What's your experience with remote work? What are some of the practices you've developed to be effective? What practices from the company have you experienced that were helpful or detrimental?
1. Tell me about a colleague with whom you've worked in the past who you thought was truly exceptional. What qualities or practices did they demonstrate? In what ways are you similar to them, and in what ways are you different?
1. Please consider the best and next best teams you've worked with. How would you characterize them? What did they have in common, and what distinguished the best from its next counterpart?
1. What did you observe in our last sprint update?
1. What questions do you have for me?

Note that this is not necessarily exhaustive. We may also explore specific questions that arose from my review of your experience, your fit with DoubleGDP, or discussions from previous rounds of interviews.

#### Advice on how to interview well with the CEO
Here are some tips on how to have a good interview with the CEO:

- Be concise. Answer directly and specifically; let him ask follow ups as necessary.
- Be willing to ask for clarification if needed. 
- Be honest and straightforward. Don't try to spin or obfuscate, even if he probes in areas you're not proud of.

### Investor Interview
Some candidates will be required to meet with our investor. Before sending interview invitations, it's important to prepare a summary doc with relevant information. The interview doc is internal and should **not** be shared with the candidate.

**Note for Candidates**
We recommend you prepare for your conversation with the DoubleGDP Investor. You can browse the [GitLab CEO Handbook page](https://about.gitlab.com/handbook/ceo/) and in particular review the [pointers from his direct reports](https://about.gitlab.com/handbook/ceo/#pointers-from-ceo-direct-reports) and context on [how he conducts interviews](https://about.gitlab.com/handbook/ceo/#interviewing-and-conducting-meetings).

Our [advice on how to interview](#advice-on-how-to-interview-well-with-the-ceo) with our CEO also applies.

**Notes for Internal team**
Use the practice below when scheduling on your calendar directly or on behalf of someone else:

* Create a separate Google doc for each interviewee
* Add a link to the doc from your calendar invite

Include in the doc:

* Candidate's name and current title
* Candidate's email address
* Link to candidate's LinkedIn profile
* Link to DoubleGDP's reference check
* Summary of any particular points to address or known flags to probe on.

## Candidate Frequently Asked Questions

Here are frequently asked questions and our answers:

1. What's the latest news?
    1. We post updates every two weeks on our [Twitter page](https://twitter.com/2xgdp)
1. What’s is your funding situation?
    1. We’re funded by a single investor and not expecting revenue immediately. Our goal is to demonstrate product market fit through resident growth and operational customers, and raise in that timeframe.
    1. Our investor is Sid Sijbrandij, the founder and CEO of Gitlab. He has built a successful all-remote company that provides an end-to-end platform in a market full of fragmented solutions. In addition to funding, he provides advice and relationships to help us learn from and build upon Gitlab’s success.
1. What’s your stance on remote work?
    1. We’re committed to being an all-remote company with no offices. We think this yields better results: it will build a culturally diverse workforce necessary to operate in cities around the world, offers great advantages to employees like control over personal time and no commute, and is much more productive than semi-remote environments that disadvantage remote employees over those at headquarters.
    1. This does take an investment in the process to make work, for example: good hygiene around documented communications, a handbook that’s operationally useful and gets contribution from the whole company, and team social chats -- agendaless meetings just for socializing. These are good practices anyway, but many companies don’t invest sufficiently because so much of their process evolves informally.
1. How many teammates are you?
    1. Our current team is listed on our [website](https://www.doublegdp.com/about/)
1. What’s your value proposition?
    1. For cities, we help them attract and build relationships with residents, deliver and improve municipal services, and have an innovative and cost-efficient software infrastructure.
    1. For residents, we provide access to all city services in one place and a way to give feedback and be heard. We expect this to build strong communities and better places to live.
1. What’s the compensation range?
    1. We aim to pay at the 50th percentile of the local market in which we're hiring. We factor in the role, seniority, and location. Your exact compensation amount will be based on your experience level and market conditions. Note that we do not offer options/equity at this time. Some references we use to calibrate on market rates are reports from the [Birches Group](https://birchesgroup.com/), [Payscale.com](https://www.payscale.com/), [Glass Door](https://www.glassdoor.com/), [Salary.com](https://www.salary.com/), and [Blind](https://www.teamblind.com/).
1. Do you offer options/equity?
    1. Starting in 2022-03, DoubleGDP implemented an Equity Incentive Plan (EIP) in order to allow team members to have ownership in the company and to benefit from the wealth that could be created through the company’s long-term success. 
1. Have you specified growth targets and timelines? How well are you doing against those goals?
    1. Please see details on our [goals page](https://handbook.doublegdp.com/company/goals/#company-goal)
    1. Please see progress against top-line goals from our latest "Progress Update" from our [YouTube Playlists](https://www.youtube.com/channel/UCALY7l5iisNVrEyvLgQa3ig/playlists).
1. Is there a defined process that will guide how customers will transition from the current partnership status to revenue generating status?
    1. Our plan is to grow revenue along with our partner cities' revenue. This aligns our incentives with the theirs: to create economic growth. As an end-to-end platform ([see vision here](https://docs.google.com/presentation/d/1c-Ci9QVSc-H0vwtelS0mL4Ka5v2kbzr-1BfOSfLp4d4/edit#slide=id.g5137e6e1a9_0_244)) there are many opportunities to monetize. We work with our partners to consider the best approach for them, and have explored transactions, revenue share, and monthly fees based on population.

## Tips for a successful interview
**Test your tech in advance** There’s nothing worse than having technical difficulties during an important call. That’s why it is important to get familiar with [Zoom](https://zoom.us/) before your interview.

* To begin, download the app to your phone or desktop ahead of time. If you’ve never used Zoom before, familiarize yourself with the quick start-up guide for new users. See [bandwidth requirements](https://support.zoom.us/hc/en-us/articles/201362023-System-requirements-for-Windows-macOS-and-Linux )

* Once you have downloaded the Zoom app, take a moment to start your own private meeting to get familiar with the interface and features.

* You can take your test one step further by recording yourself and watching it to find areas of improvement.

* Ensure that you have a stable internet connection. Use an ethernet cable if necessary. You can check your bandwidth through [SpeedTest](https://speedtest.net) or [Bandwidth Place](https://www.bandwidthplace.com/)

* Test out your sound. Use headphones to hear clearly and to block outside noise.


## References
We request 5 references before making an offer, of which we will speak with at least 3. This helps us learn more about you from those who have worked with you closely. In addition to the obvious benefit of helping us assess what you can bring to the team and gain context on who you are as a person, it also helps set the stage for a productive working relationship with you because we can ask advice on how best to support your success once onboard.

We would like to talk with a current or former colleague from each of these categories. An individual from each of the first three categories is required, and the fourth if you're interviewing for a supervisory role.

1. Your direct (or formerly direct) supervisor - someone responsible for or who oversaw your work
1. A peer in the same role as you - someone who did similar work to you
1. A cross-functional partner or customer - someone who was a consumer of your work
1. Someone you've supervised, if you're applying for a management position
1. Someone else of your choosing

We encourage you to share people who have worked with you closely and know your capabilities well. We also encourage you to ask them to be candid so we can get to understand who you are and what you can bring to the team. We have a [reference script](https://docs.google.com/document/d/1xNIPigVRPgoZZX5fkLvsBY9Dgt77za6pgyIunz33PVs/edit#heading=h.hh2zfqnu4rt) that we use, but do not share in advance because we want candid and spontaneous responses.

## Offer

Offers should be delivered verbally, then with an email attaching a signed [Offer Letter](https://docs.google.com/document/d/1Amc2qu1UceRi_IKii6ThCQnEWqDDRO7Iz4I7Cl97Zj8/edit), and then with a legal contract. Some team members join us through an agency, in which case the details are worked out in the conversation with their agency. For independent, full-time team members, use this [offer template and script](https://docs.google.com/document/d/1ph51ZB_Cg1p3gkK47I_yBmQ1yEG_cVcwjtHGYdlTC8Y/edit).

## US Based Consultants

When retaining the services of a U.S. based consultant, please use the following [Consulting Agreement](https://docs.google.com/document/d/108h0ksfFGc0J6u2MvDLJG9-hpR3W_LcJ6X-XBGInVSw/edit)

Follow these steps to fill in the Consulting Agreement:

1. Change the '**Effective Date**' located on PAGE 1 under the Title.
2. On PAGE 8
   1. Add the name and title of the Employee executing the Agreement in the '**CLIENT:**' section.
   2. Add the name on the consultant in the '**CONSULTANT:**' section.
 3. On Exhibit A (PAGE 9)
     1. Fill in the '**Project Assignment: #**' Section with the appropriate Project NUMBER below the tile.  For Engineering, project numbers starts with E followed by a NUMBER.
     2. Fill in the '**Dated:**' Section with the appropriate date below the tile
     3. Fill in the date the work will start and the date it will end under '**Schedule Of Work:**'
     4. Set the hourly fee in Section A of '**Fees And Reimbursement:**'
     5. Set the maximum amount we may pay the consultant for the project in Section C of '**Fees And Reimbursement:**'
    6. Add the Scope of Work in the '**Project:**' Section of the Exhibit A section (PAGE 9)
 4. On PAGE 10
    1. Add the name and title of the Employee executing the Agreement in the '**CLIENT:**' section
    2. Add the name on the consultant in the '**CONSULTANT:**' section on PAGE 10
    3. Fill in the '**Dated:**' Section with the appropriate date
 5. **DO NOT CHANGE EXHIBIT B AND C.**  
 6. Sending the full contract via Docusign or helloSign.
   1. Add signature lines for CEO and Contractor on main Agreement (Page 8)
   2. Add signature lines for CEO and Contractor on Agreement and Exhibit A (Page 10)
   3. DO NOT TOUCH Exhibit B and C — these are for reference only
   4. Send to CEO and Contractor for signature
 7. Email [peopleops@doublegdp.com](mailto:peopleops@doublegdp.com) to inform them to set up the consultant with a 1099.
 1. Save the executed copy of the Agreement to our Shared Drive for contracts. (Ask CEO for link.)
