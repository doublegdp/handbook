# Overview

## Purpose

This document aims to capture all processes relevant to DoubleGDP and grow into our version of the [GitLab handbook](https://about.gitlab.com/handbook/). It's meant to be a collaborative document that every DoubleGDP teammate uses and contributes to for all company processes and procedures. As an all-remote company, clear documentation is essential to our effectiveness, and this handbook makes it easy to reference with deep links so that the team can use it as a short-hand to share expectations of how to approach a task, and for all to update regularly as we improve our approach.


## Editing the Handbook

The handbook is stored in a [GitLab repository](https://gitlab.com/doublegdp/handbook). If you have write access to the repo you can make edits directly there. However, you're strongly encouraged to set up a local environment so that you can make more substantive changes more easily.
