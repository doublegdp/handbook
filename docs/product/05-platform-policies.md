---
title: Platform Policies
---
## Overview

By accessing or using the DoubleGDP website and platform, you agree to be bound by our policies. For any questions or concerns regarding our Terms of Use, Privacy Policy, and User Data Deletion please contact us at [support@doublegdp.com](mailto:support@doublegdp.com).

### Terms of Use

Our “Terms of Use” govern the use of the DoubleGDP, LLC website and platform, including all content or information hosted therein and any subdomains and services thereof.

<iframe src="https://docs.google.com/document/d/e/2PACX-1vQfiUzI44PucBSGAeZ_BQ8pFI5ETkrEU5yMZ9dubyekeGQMdbQsReXfruIAh48FMjYPPLlwFJtquzgL/pub?embedded=true"style="width:100%; height:300px;"></iframe>


### Privacy

We are committed to protecting your personal information and your right to privacy. Our privacy policy highlights what information we collect, how we use it and what rights you have in relation to it.

<iframe src="https://docs.google.com/document/d/e/2PACX-1vTWhDBjbiKHJOW-ly8ZG7sTsnCwgV926fqGuokPJX6wHpFQalKKi7L0bpFrIdlR1eftKxXCqZYbitg0/pub?embedded=true"style="width:100%; height:300px;"></iframe>



### User Data Deletion and Copy

To request the deletion of your account and user data or to receive a copy of your data for personal records, please contact us at [support@doublegdp.com](mailto:support@doublegdp.com) with the following information:

- Full Name
- Email
- Phone Number
- Place of residence (Country, State, City)
- Type of Request: Data Deletion or Data Copy
- Any other pertinent information to identify account

Your name, email, and phone number is used to identify your account, while your place of residence is used to identify the rules that regulate your data.

Once we receive your request, a support representative will reach out to you promptly to get any additional information to process your request. After the request is processed, you will receive an email to confirm your data has been deleted or a copy has been provided for your records.

### User audience for apps

The DoubleGDP app is intended for all community members over 18 years of age in a city.

Today we serve only adults who are 18 years or older. We are open to considering access for those under the age of 18 after consultation with and consent from parents.

## Customer Instances
Each customer of DoubleGDP has their own instance of the platform, so that to their community the app looks like it belongs to the city. It has the city logo at the top of the page and will leverage the city brand colors.

### Customer URLs
We provide a unique URL for each customer. They are as follows:

* DoubleGDP Demo Site - [demo.doublegdp.com](https://demo.doublegdp.com/login)
* Nkwashi - [app.doublegdp.com](https://app.doublegdp.com)
* Tilisi - [tilisi.doublegdp.com](https://tilisi.doublegdp.com)
* Enyimba - [enyimba.doublegdp.com](https://enyimba.doublegdp.com)
* Ciudad Morazán - [morazancity.doublegdp.com](https://morazancity.doublegdp.com)
* Greenpark - [greenpark.doublegdp.com](https://greenpark.doublegdp.com)
