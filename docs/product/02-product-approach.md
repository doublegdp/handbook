---
title: Product Approach
---

## Principles

We adhere to the following principles as we build and grow our product offering: <br>
1. **Iterate –** Scope down large problems. We take thousands of small steps and guide ourselves based on what features get used. <br>
2. **Avoid Premature Optimization -** We often start by doing things manually, then progressively automate. We maintain a bias toward immediate action which helps us avoid wasting resources, especially in situations where we have incomplete information. <br>
3. **Build Mobile First –** Every interaction with our product should be possible and comfortable on a mobile device. <br>
4. **Simplify -** Be simple and straightforward in language and UI. We aim to concisely describe concepts in language that can be easily understood. <br>
5. **Prioritize Convention over Configuration –** Wherever possible, create product workflows that require configuration only when someone wants to explicitly override a default. We intend for most users to be able to use our product "out of the box" without customization. <br>
6. **Follow Industry Paradigms -** Nothing we’re doing hasn’t been done before. Our value and where our critical thought should be is in integrating ideas from various tools into a cohesive whole. <br>
7. **Create Reliable Data –** Every action in our system should be logged so that data can be relied on and changes can be audited. We should have minimal required input fields to avoid capturing junk data. <br>
8. **Be Transparent and Encourage Feedback -** Nearly all of our work is open and public which allows everyone to operate with the same information. This also enables our subscribers to provide the most relevant feedback as part of our iterative process. <br>

## Product Discovery

### Purpose
To add a new feature to our product, we use the product discovery process with the goal of learning as much as we can about the target audience and understanding the problem we are trying to solve. The intention is to reduce uncertainty around the problem to give us the best chance of building the right product for the right audience.


#### Stakeholders
Key stakeholders within DoubleGDP involved this process are Head of Product, Head of Engineering, designer, lead engineer for proposed project and a customer success manager.

#### Steps
#####Identify end users
Figure out who the end user(s) for the feature will be. This should be someone whose day to day work is directly affected by the problem we are trying to solve.

##### Arrange for interviews
Make plans to make a site visit if possible, or arrange for a remote meeting. The best option is to meet the end user and observe them as they go about their work normally. It is advisable to record the session if possible. Create a google document and take notes as you observe. Pay keen attention to how they conduct their work and ask questions. To give you some ideas of what questions to ask, here are some resources:

- [Customer Discovery: What do you ask](https://www.youtube.com/watch?v=OTkP2JDeGWM)

Take note of any resources that might be useful references, for example physical documents. Take pictures or if possible, have the user send the documents where they can be referenced later.

At the end of this process, we should have a clear picture of what the existing process looks like, what works best and the key pain points from the client's perspective.

##### Debrief with stake holders
After the interview, all relevant stakeholders should have a meeting where they discuss the findings of the interview. All materials should be shared in a google drive and reviewed prior to the meeting. Stakeholders will discuss the best solution. All notes, concerns or questions arising from the meeting should be recorded in an epic.

##### Protoyping and design
After deciding on the way forward, the designer will create mockups or wireframes to help communicate the agreed on solution to the client. This could involve several iterations based on feedback from other stakeholders.

##### Validating
When the mockups are finalised, they are presented to the client preferably with the end users present. More feedback is gathered on whether the solution works, possible improvements and any other important information that could influence the final product.

##### Development
If the proposed solution is agreed on with the end users, development can proceed. The epic is updated with designs any other information necessary for development. The epic is decomposed to smaller tickets which are added to sprints to be worked on by engineers.
