---
title: Handbook Setup
---

## Turn On Notifications

In order to make use of the Handbook it's important that you are able to receive notifications from GitLab. Here are instructions on how to configure this:

1. Open `Settings` from your profile drop-down at the top right of [GitLab](https://gitlab.com)
1. Select `Notifications` from the `User Settings` menu on the left nav bar
1. Confirm that the `Notification email` has your @doublegdp.com email address
1. Set the `Global notification level` to `Participate`
1. Confirm that all of the `Groups` and `Projects` on the lower section of the page are set to `Global`

This will configure GitLab to send a notification email on all tasks and changes for which you are a participant or @mentioned, and it should be your default setting. You may modify these settings to better suit your preferences (instructions are available in [GitLab documentation](https://docs.gitlab.com/ee/user/profile/notifications.html)), but it is your responsibility to stay aware of requests made of you through GitLab.

*Screenshot illustrating the proper notification settings...*
![GitLab Notification Settings](../img/handbook-notifications.png)


## Set up a local copy
You can contribute entirely through the online process above. However, you may also wish to set up a local copy of the handbook. This will make it easier to preview your changes and also gives you access to a few other writing tools that make it more user friendly. There's a bit more complexity to this process, but this guide should get you going.

To set up a local version of this handbook, you will need at least the following set up on your machine:

1. Git -- see these [installation instructions](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
1. A Markdown editor -- we recommend [Typora](https://typora.io/) for a simple visual experience or [Atom](https://atom.io/) if you want an environment integrated with git.
1. A local copy of the repository (instructions below)

This setup will be sufficient to make changes to the documentation, and is user-friendly for non-developers. You'll be able to see and contribute the files within the documentation, but won't have the same UI as on the website. If you want that full experience, you please install [MkDocs](https://www.mkdocs.org/).


### Setting up Git
You will need to set your credentials in git. This may be a bit daunting at first, so we'll step through it together. Instructions are [here](https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup)

<details>
<summary>Git setup for Mac</summary>

<p>From your terminal, run the following:</p>
<ol>
<li> <code>git config --global user.name "John Doe"</code></li>
<li> <code> git config --global user.email "johndoe@doublegdp.com"</code></li>
</ol>
</details>



### Local copy of repo
To create a remote copy of the repository, open your terminal interface and navigate do the directory where you want to store it. (Nolan suggests `~/Projects`.) Then run `git clone https://gitlab.com/doublegdp/handbook.git`; this will create a `handbook` directory that has the latest copy of the project.


### Script to initiate your local Copy

CEO finds it helpful to be able to launch the handbook with a single phrase -- e.g. pull from git, open the IDE, and start the web server. Here is a script that can help with this. This script is saved in `~/.bash_profile`; note that on most Macs, zsh is now the default shell and so you may need to adapt this to your environment.

```
# function workon() { gitupdate; cd ~/Projects/$1 && atom . && mkdocs serve; }
function workon() {
  if [ $1 = "web" ]; then
      cd ~/Projects/dgdp_web
      git pull
      atom .
      bundle exec jekyll serve
  elif [ $1 = "app" ]; then
      cd ~/Projects/$1 && git pull ;
      echo "open -a Docker"
      echo "docker-compose up"
  else
    cd ~/Projects/$1 && git pull && atom . && mkdocs serve;
  fi
}
export -f workon
```



## Make your first edit
To make your first edit:

1. In your terminal, run `git pull`
1. Open your markdown editor from the `docs/` directory within the Handbook
1. In your markdown editor, make a change and save it
1. In your terminal run `git add .` or `git add docs/name_of_changed_file.md `
1. In your terminal, run `git commit -m "Make first contribution"`[^commit]
1. In your terminal, run `git push`


## Run locally

If you want to run the handbook locally so you can see edits before committing, you also will need to set up your local environment to run mkdocs and other dependencies for the handbook. Setting up a local environment sometimes is easy and we'll outline the simple steps here. Unfortunately, complexities can sometimes arise based on other things in your environment; if this happens try Googling the error message and see if you can easily resolve it. If not, reach out for help!

The basics that you'll need are:

1. Some installer -- we recommend [Homebrew](https://brew.sh/)
1. [MkDocs](https://www.mkdocs.org/) -- try `brew install mkdocs`
1. [Mkdocs Material](https://squidfunk.github.io/mkdocs-material/) theme -- try `pip install mkdocs-material`
1. [Material Extensions](https://pypi.org/project/mkdocs-material-extensions/) -- try `pip install mkdocs-material-extensions`
1. Click [Here](http://localhost:8000/) to access your local copy of the Handbook.

Note that in order to do this, you will need to know a bit about how to use the command line within the `Terminal` app (assuming you're using a Mac). Here's a [quick primer](https://www.theodinproject.com/paths/foundations/courses/foundations/lessons/command-line-basics-web-development-101).


### Notes on updating / upgrading local environment

1. Make sure to periodically update and upgrade `brew`: `brew update && brew upgrade` then `brew cleanup`
1. Note that I first had to update all of my command line tools. (I don't remember the link to these instructions)
1. In order to update brew, you may need to `git -C /usr/local/Homebrew/Library/Taps/homebrew/homebrew-core fetch --unshallow`, which grabs the latest list of tools. Then I ran `brew upgrade`. Based on the system's warning I also did `brew link --overwrite python@3.9`
1. Update mkdocs: `brew upgrade mkdocs` failed, so I ran `brew install mkdocs`. This process also required `brew link --overwrite mkdocs` since there was a previous version.
1. Install `mkdocs-extensions` by doing `python3 -m pip install mkdocs-material`

### Updating local environment to run website

The website runs using Jekyll instead of MkDocs.

1. Install rbenv: `brew install rbenv`. Use Homebrew to install rbenv, but use rbenv to do the rest of the install.
1. Install Ruby: `rbenv install 2.6.3` then `rbenv global 2.6.3`. Note that the website has dependencies that have not been updated in the latest version of Ruby; 2.6.3 is known to work.
1. Install Jekyll: `gem install bundler jekyll`. Doing this required `brew install openssl`
1. `bundle install`
1. `bundle exec jekyll serve`
1. Click [Here](http://localhost:4000/) to access your local copy of our website.






[^commit]: This creates a 'commit' with a message. A `commit` is an individual change to a set of files. Your message should be a brief and concise description of a simple change you're making to the docs. Make the first word of your commit message a verb, and use present tense.
