# Nolan's hello

I love Beethoven and The Beatles. Both, in part, because of their music, and because of how much they changed the course of music for all the artists that came after them.

I also love biscuits and am not afraid to admit it. They're especially good with honey butter. :)

I like to teach, and enjoy writing in Markdown. But teaching on a zoom call has some challenges...

I like spreadsheets.


I like to teach how to use Git and GitLab. I also like using Atom as my IDE.

I want to show how to do two commits with one push.

John is one of Nolan's favorite mentors


I can reference Cecilia's hello world in a few ways:

* I can make an [absolute reference](https://handbook.doublegdp.com/handbook/hello-world/cecilia/)
* I can do a [absolute project-level reference](/handbook/hello-world/cecilia/)
* I can also do a [**relative** project reference](../cecilia/)
* e.g. I can also go up two levels to see the [setup instructions](../../setup/)


<details><summary>Does this work?</summary>
    I want to confirm my understanding of the details element. Let's see if this worked.
</details>
