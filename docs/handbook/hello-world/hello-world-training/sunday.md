Hello World

My name is Sunday and I am from an amazing city called ***Nairobi.*** The name Nairobi comes from the Maasai phrase ***Enkare Nyrirobi*** which translates to ***the place of cool water*** .Who wouldn’t want to live in the most amazing city in the world? Nairobi, popularly known as “Green City in the Sun” has an amazing culture, which is unparalleled to any other city. The city is known for its sheer natural beauty that will take your breath away. Nairobi is a place where everyone collides into a magnificent experience that will take a lifetime to forget. A lot goes on in Nairobi, from entertainment to business.

Here are reasons why Nairobi is the most amazing city world

 * Nairobi is steeped in rich culture, heritage and the people are very welcoming.

 * Amazing city with vibrant nightlife. Nairobi’s nightlife represents some of the city’s legendary scenes. You will love it!

 * Nairobi offers a great shopping experience and an opportunity to find beautiful goodies that you can ever imagine both locally and International made goods.

 * Nairobi has some exceptionally beautiful natural parks and gardens such as Uhuru parks, Nairobi Botanical Garden. These parks and gardens are beautifully landscaped with lush green lawns and lined with trees. You will be pleasantly surprised by the tranquillity that you can rest in at these parks.
 
 * You will find Kenya’s most important art museums, featuring masterpieces of African art from centuries. 


