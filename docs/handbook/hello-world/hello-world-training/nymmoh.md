
## NAIROBI 

Welcome to the home of diversity the “place of cool waters” **Nairobi**. My people call it “*nairovi*” The young people like to call it just,” *Nai*” or “ *Mtaani*”, While others call it “The City in the sun”.

* Many are the names in reference to this hub of our Kenyan pride. But all this refer, to this single place Nairobi, My home, My pride 

It is at the centre of the heart of Kenya, a city that prides itself on a rich culture the culture of the diverse tribes of Kenya. The forty-two tribes united under one common umbrella called Kenya.

It is a city that prides itself on a rich wildlife preserve, the only city in the world with a national park. The Nairobi National Park is one of the few places you will be lucky to view the big five of the African animals.

Karibu to Nairobi, just know, “Karibu” is a swahili word for welcome, so any time, you are welcome to Nairobi
