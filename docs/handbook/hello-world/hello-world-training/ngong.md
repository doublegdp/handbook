### Ever heard of the famous Ngong Hills? 

Well... allow me to indulge you for just a bit.

**Ngong** is a town near the Ngong Hills along the Great Rift Valley within Kajiado County, in southern Kenya. 

The word ***Ngong*** is a Maasai word derived from the word ***enkong'u*** meaning *the **'eye'** of water*. 
- The Maasai usually refer to a spring as the *eye* of water. 

The Ngong Hills, from the east-side slopes, overlook the Nairobi National Park game reserve and, off to the north, the city of Nairobi. The westside slopes overlook the Great Rift Valley dropping over 4,000 feet below.

###### Like Climbing? 
The Ngong Hills should be your next attraction while on a vacation in Nairobi City. 
* The climb to the seven hills of Ngong is very good experience for a first time climber.

Next time you are in Nairobi, and you fancy a rich Maasai cultural experience, look no further than the magnificent Ngong town.

Karibu Ngong!
