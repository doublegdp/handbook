Hello World,

I live in a beautiful city known as the Lekki Penninusula. It is located in the south eastern part of Lagos adjoining Victoria Island and Ikoyi districts. 

Lekki accomodates a mixed population (commercial and residential) of about 3.4million people. The city is well developed with a number of gated residential estates, heathcare centres, schools / educational facilities, agricultural farmlands, recreational centers and areas allocated for the free trade zone. 

The tourist sites and monuments within Lekki City include:

* Eleko beach

* Dream World Africana

* Iwalewa art gallery

* Yemisi Shyllon museum of art, Pan Alantic university

* Filmhouse Cinema

* Lekki Conservation Centre

The city is known for its liveliness, being a hub of socialites, and its idyllic beaches. Let me know when you are coming to my city so I can show you around!

