Hello World!

I live in Westlands, Nairobi.
Westlands is home to some of the tallest buildings in the city. The most recent city jewel is the GTC tower.

Most Nairobi city residents refer to Westlands as uptown and home for the affluent.

Some of my favorite things about Westlands are listed below;
* Proximity to the CBD.
* Great malls, different cuisine restaurants, movie theaters and recreational parks within walking distance.
* Westlands is referred to the entertainment center of the city.
* Fancy office buildings and residential property making for a classy look and feel.
* Quick access to basic amenities like medical centers, food markets and schools.

If ever in Nairobi, be sure to check out the K1 Flea Market that happens on Sundays.

KARIBU WESTLANDS!
