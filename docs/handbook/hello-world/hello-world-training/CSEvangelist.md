Hello World,

I am your new CS Evangelist. I live in Abuja, the capital of Nigeria. It lies in the central part of Nigeria, in the Federal Capital Territory (FCT), with a total population of 2.5 million+.

Abuja currently has the 4th largest metropolitan population in Nigeria after Lagos, Kano and Ibadan, created in 1976 and is approximately 300 miles (480 km) northeast of Lagos, the former capital (until 1991). 

The site was chosen for Nigeria’s new capital because of its central location, easy accessibility, salubrious climate, low population density, and the availability of land for future expansion.

Abuja is a beautifully planned cosmopolitan capital city with an extensive highway network, appealing green spaces, colourful nightlife and many restaurants. 

Also, the climate in Abuja is tropical, and the weather is usually warm, with bright sunny days being normal.

The beautiful city of Abuja and the sights, makes it truly the capital city of Nigeria. 
