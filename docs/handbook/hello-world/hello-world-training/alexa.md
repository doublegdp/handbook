Hello World!

My name is Alexa and I live in a city called Nairobi.

It is a beautiful city in Africa and is the capital city of Kenya. 

It is popular for the following things :

* It is the only capital city in the world with a national park in it.

* It has amazing hangouts and the people are quite friendly.

I love the Nairobi culture and there is no other place I would rather call home.
