
<h5>My Work Style</h5>

I am Mark Otieno and please see below my work style:

I am able to adapt to any work environment, I can work in a team or independently. Naturally, I always strive to make an impact in my interactions be it with customers. This attitude extends to my workplace and makes it easy for me to collaborate with colleagues. 

**Mondays** - Setting week's deliverables and engagements<br>
**Tuesday - Thursday** - Working on deliverables and customer engagements<br>
**Focus Fridays** - Focused on reports and tying up on unfinished tasks and deliverables

I find myself more productibe in the mornings, I leave my evenings for meetings and catch-ups.

I treasure constructive feedback, a great way for me to learn and deliver better results. It's often difficult to assess one's work objectively, so I value the input of my manager and colleagues.
