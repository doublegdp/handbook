---
title: Handbook Skills
---

This page covers some basic concepts that will help someone new to using the handbook understand what they're doing. We recommend you start with the [Overview](index.md) to understand the reason for the handbook and our process for making and proposing edits. Once understood, this page should help you master some of the skills you need.

## Contribute to handbook basics

Here are the basics needed in order to be able to contribute to the handbook. Please leverage this like a lesson, and make your first contribution by visiting the [online editor](https://gitlab.com/-/ide/project/doublegdp/handbook/tree/master/-/docs/) ("Web IDE") within GitLab. There are a few key concepts to understand:

1. The "IDE" ("Integrated Development Environment") has all the files for the handbook and allows you to edit them using a language called [Markdown](https://markdown-guide.readthedocs.io/en/latest/basics.html). It functions mostly like plain text, but you'll want to have a basic understanding of [what it is](https://en.wikipedia.org/wiki/Markdown) and its syntax.
1. Once you've edited files, you need to "commit" them. This is sort of like saving it, but adds a comment so that you can explain briefly what edit you've made. This is called a "commit message" and should be just a few words, start with a verb, and be written in present tense. For instance, if you updated this page to add another step to this setup you could say "Add step to handbook setup".
1. In order to commit, you should use the "master" branch. If you don't do this your changes won't appear on the website, but they won't be lost -- an engineer can help you recover them and add them in. However, it's better if you put them straight to `master`. This is not the default behavior for GitLab, so you need to explicitly select it. See this diagram for what to look for.

![Commit to Master](../img/commit_to_master.jpg)

That should be enough to get you going! In fact, we recommend you use this process right away to make your first contribution by creating a ["hello world" page](../hello_world/) so that you get comfortable with the process.

## Make more sophisticated contributions

In order to make more sophisticated contributions to the handbook, for instance to propose improvements to our process, you need to be familiar with these terms:

* Repository ("Repo") - the set of files that comprise the handbook source code. These are the files [stored in GitLab](https://gitlab.com/doublegdp/handbook/-/tree/master) rather than the [published version](https://handbook.doublegdp.com/)
* Master and Branch - `master` is the latest set of files that are published. "Branches" allow us to make edits on a temporary copy so that we collaborate on it before publishing. See a more complete [explanation below](#master-and-branch).
* Commit - the act of saving a change to a file (or multiple changes to multiple files) is called a `commit`. An edit can be committed to `master` to be published immediately, or to a `branch`, in which case it will be saved but not published until that branch is merged into to master.
* Merge Request ("MR")- a `branch` is "merged" into `master` once it is ready to be published, which signifies that it has been approved and its contents are officially part of our company process. A "Merge Request" is a workflow tracking concept in GitLab that allows us to describe, discuss, and manage a set of changes that are being proposed.
* Issue - an `issue` in GitLab also tracks workflow and may have a description, discussion or other metadata that helps track it to completion. It is similar to an `MR`, but should be thought of as a problem (or question) that gets resolved (or "closed") by an `MR`.  Confidential issues should be used when we reference PII (e.g., any individual person's name) or any named partner other than Nkwashi. Please refer to GitLab's documentation on [confidential issues](https://docs.gitlab.com/ee/user/project/issues/confidential_issues.html). Examples of the proper way to use this are discussed below.

Please try this now. Make a change to the "hello world" you created in the previous step, but this time commit it first to a branch and create a new merge request. This will be sent to a manager for approval, so add a description and @mention them to request them to approve the change.

![Commit to branch](../img/commit_to_branch.jpg)


### Master and Branch

It will be helpful to have a richer understanding of `master` and `branch` in order to do more with the handbook. This diagram illustrates how `master` evolves through a series of commits, with both a simple and compound `branch` that capture changes. In this example, imagine that three types of changes are being committed.

* A trivial change (like fixing a typo) is committed directly to `master`;
* A simple change (like adding a step that you've already discussed with your manager) is made on a separate branch and then approved and merged back to master
* A compound change (like defining a new procedure that needs input from a few teammates) has been started and not yet merged back.

Note that in the illustration, an additional trivial change is made to `master` at the same time that simple change is made on a separate branch. GitLab will (in most cases) manage to merge this back with no problem -- this is what allows for asynchronous development of processes.

![Master and Branch](../img/master_and_branches.png)

### Miscellaneous

The handbook has support for [Mermaid Flowcharts](https://mermaid-js.github.io/mermaid/#/). Nolan will give super-props to the first person who uses this functionality for any semi-legitimate reason. :)
