## Implementation Manager
DoubleGDP asks its collaboration partners to designate a dedicated Implementation Manager to support facilitation. This is a role external to DoubleGDP that can be part-time or full-time. The job description below can aid in identifying a Implementation Manager internally, or hiring one externally.

### Description
As the Implementation Manager, you are responsible for increasing, facilitating, and assessing user adoption of DoubleGDP software within our city. The Implementation Manager serves as the primary point of contact between our company and DoubleGDP, our city software provider. You help our company utilize DoubleGDP software, and help the DoubleGDP product team understand how to improve the platform to better serve our needs. You develop meaningful relationships with residents, other community members, and regulatory stakeholders to understand and articulate goals, train people on how to leverage the software, and ensure increased adoption across these groups. This role is a vital to the success of our city, and a key to shaping our city software.


### Responsibilities
- Training and rollout of new features
- Define user and stakeholder goals and plans
- Establish a regular communication cadence with key account stakeholders
- Collect user and stakeholder feedback and share in a structured way with the DoubleGDP customer success and product team
- Participate in quarterly business reviews with DoubleGDP


### Requirements
- 5+ years of work experience
- Excellent verbal and written communication skills
- Proactive mindset, looking for ways to improve and willing to advocate on behalf of users
- Able to quickly understand DoubleGDP's application and relate it to user experience
- Demonstrated patience in dealing with customers
- Bachelor's degree is expected
- Familiarity with Google Docs, Sheets, and Slides; Slack and Asana is required
- Experience within, or working adjacent to, product teams is a plus
- Experience in training or teaching is a plus
- Familiarity with other SaaS software for CRM, ERP, security, or finance is a plus
