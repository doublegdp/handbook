## DoubleGDP Partnership Approach

We approach our partnerships with the following access and services, and by doing so, establish a foundation of trust and more fruitful partnership. As our partner, you receive:

1. Data Control: You have control of, and full access to, your data
1. Source Available: You can inspect the software at any time
1. Open APIs: We work with you to connect and integrate your critical software
1. Modular: You can select DoubleGDP functions based on your needs
1. Whitelabeled: Your logo and branding are front and center
1. SaaS Architecture: Our software scales with your growth


### Co-Design Collaboration Principles
Great software comes from real-world usage. Our approach is geared to maximize and accelerate software adoption and the population growth of the city:

1. DoubleGDP develops its end-to-end platform closely incorporating our Partner's needs. Our Partner uses the platform and shares feedback with DoubleGDP. 
1. DoubleGDP designates a customer success manager to support each relationship. Our Partner provides a point person committed to driving value and user adoption of the DoubleGDP platform. Our Partner is also supported by DoubleGDP’s globally distributed product management, design, and software teams.
1. DoubleGDP owns all software (including all user interfaces, designs, and other related materials) that it develops.
1. DoubleGDP typically begins releasing software co-designed with the Partner within six weeks of launching the collaboration. DoubleGDP works in semi-monthly sprints and releases new features each sprint.
