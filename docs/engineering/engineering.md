# Engineering

#### Team Values
 - Coming soon

#### Development Team Rules

- Process Etiquette:

  - Keep commit messages clean and readable.
  - Keep branch name and bugs readable.
  - Keep gitlab branches clean (remove old branches)
  - **No MR without a Story or a bug.**
  - MR must contain the link to Gitlab issue
  - Any issue that consists of both Frontend and Backend changes, an MR should only be made when both changes are completed.
  - **1 Branch per Story.**
  - Only work on one story at a time
  - An issue is owned by only 1 engineer, but an issue can have multiple collaborators
  - An engineer who is the owner of an issue must ensure the issue functions properly before merging to master.
  - Do not keep a Story for more that 2 days in Build/Merge/Staging/verify without team consensus.
  - Create a MR only when you are ready to merge.  Avoid 'Draft' and 'WIP'
    - Use the default MR template available on gitlab when creating a MR
    - Attach a loom video of how your code works if it involves visual changes.
    - Reach out to another engineer to help verify your feature in staging before marking it as "Staging Verified"
    - Add 2 reviewers when creating the MR.
  - If a bug is identified on staging for a story, move that ticket back to todo and do not create a new ticket for the bug
- Code Etiquette:
  - Always leave code cleaner than when you first found it. Refactoring is good.
  - Blockers - Do not spend more than 10 mins.
  - ABT (Always Be Testing)
    - Test coverage on new feature must be 90% or above.
    - Test coverage on existing feature must never decrease.
  - Follow [Rails resource routing](https://guides.rubyonrails.org/routing.html#crud-verbs-and-actions) convention when designing React pages or modals. E.g `/labels`, `/labels/:id`, `/labels/new`, etc.
  - Do not delete records, create a `status` field and flag them as 'deleted' instead, then create a default model scope to always return undeleted records.
  - Always write code that is compatible with the current production data structure.  This means the code must work with the production data structure allowing for backward compatibility.
  - Always keep the JEST LOG and the CONSOLE LOG as clean as possible.



### Quick Links

  - [Priority Levels](https://handbook.doublegdp.com/engineering/production/support/#issue-priority-levels)
  
## Resources Available


<table>
  <tr>
   <td><strong>Resource</strong>
   </td>
   <td><strong>Description</strong>
   </td>
  </tr>
  <tr>
   <td><a href="https://docs.google.com/document/d/1AL88LBjm8_WoGSfUBKKd1k9RgWFZujbAWm4djsmHVQU/edit">Dev Ops Playbook</a>
   </td>
   <td>Deployment and debugging processes
   </td>
  </tr>
  <tr>
   <td><a href="https://doublegdp.pagerduty.com/schedules">On Call Schedule</a>
   </td>
   <td>WIP through PagerDuty. Default is: Olivier during CAT working hours, Mark during ET working hours, Nolan all other times.
   </td>
  </tr>
  <tr>
   <td><a href="https://gitlab.com/doublegdp/app">App Sourcecode</a>
   </td>
   <td>GitLab repo, publicly available. It has a README.
   </td>
  </tr>
  <tr>
   <td><a href="https://docs.google.com/spreadsheets/d/1bkmK41nazrPLcS-p7wXSXRC2fMescgfT8Ax--6FKcIM/edit#gid=101458518">Anonymized Customer Database</a>
   </td>
   <td>DB based on Nkwashi customers, but with anonymized names, fake phone numbers and NRCs, and scrambled plots.
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>
   </td>
  </tr>
</table>


## Resources We've Found Helpful


<table>
  <tr>
   <td><strong>Resource</strong>
   </td>
   <td><strong>Category</strong>
   </td>
  </tr>
  <tr>
   <td><a href="https://www.amazon.com/Web-Application-Hackers-Handbook-Exploiting/dp/1118026470">The Web Application Hacker's Handbook</a>
   </td>
   <td>Web Security
   </td>
  </tr>
  <tr>
   <td><a href="https://www.youtube.com/watch?v=2_lswM1S264">Ethical Hacking 101: Web App Penetration Testing</a>
   </td>
   <td>Web Security
   </td>
  </tr>
  <tr>
   <td><a href="https://www.youtube.com/watch?v=jrqpitsugss">Web Applications Hacking and Penetration Testing Full Course</a>
   </td>
   <td>Web Security
   </td>
  </tr>
  <tr>
   <td><a href="https://www.youtube.com/watch?v=X4eRbHgRawI">Web Application Ethical Hacking - Penetration Testing Course for Beginners</a>
   </td>
   <td>Web Security
   </td>
  </tr>
</table>
