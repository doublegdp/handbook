# Production

### State of Production

Production environment is and should be treated as a sacred place, meaning all features should always be working and tickets should have been thoroughly verified to make sure that nothing is breaking and all the processes that are crucial and currently used by our users are functioning as they should, Here is the list of features that should always be working:  
- Gate Access:  
    - Manual Entry  
    - Scan Entry  
    - Registered Guests  
- Payments:  
    - Payments Dashboard  
    - Submitting a payment  
- Forms:  
    - Creating a form  
    - Submitting a form  
- Timesheet:  
    - Start a worker's shift  
    - End a worker's shift  
    - See all workers' shifts  
- Tasks  
    - Task Dashboard  
    - Assigning tasks to a user  
- And the application overall.  

**Note: In cases production is down after a deployment,   
The best thing to do is to roll back to the previous version right away.  Before rolling back, first identify if there are any changes preventing the rollback. Also, start a Zoom call or a slack hurdle to address the situation with available teammates**

### Deploying to Production

##### All issues must have Stakeholder signoff before deploying to production
You may be deploying to production not only your own issue, but other engineers issues as well.  When this case arises, please notify them that you will be deploying shortly and they need to be ready to jump to support their issues. Make sure all the stories in staging have a label of `Staging::Verified`

Before deploying to production there are a few tests scenarios 'Gate Access Test', 'Time Sheet Check in/out Test' and Payments that must be done.   
Login credentials are in 1Password in the Developer Vault.  

Always thoroughly run the following manual tests scripts , unless Cypress ran successfully.

##### 1. Gate Access Test

**1.1 Scan** 

**1.1.1 If you don't use a mobile phone:**

- Login as a guard in incognito mode using: guard.dgdp@gmail.com
- Find yourself as a admin
- on the profile page of the selected user, in the url add this `/:tm/:dg` e.g: `/1618332435960/dg` or `/1618332435960`
    - You can use this to get the timestamp for today `Date.now()`
    - The full url should match this `/user/:id/:tm/:dg`
- select log this entry.
- Use your admin account from you regular browser to check the logs to make sure the log entry was recorded successfully.



**1.1.2 if you have a mobile phone you can:**

- Login as a guard in incognito mode using: guard.dgdp@gmail.com
- From your mobile device:
  - login as an admin on your mobile device.
  - Select the ID tile to display the QR code.
- Scan the ID code using the guard browser.
- select log this entry.
- Use your admin account from you regular browser to check the logs to make sure the log entry was recorded successfully.

**1.2 Manual Entry**

- Login as a guard in incognito mode using: guard.dgdp@gmail.com
- Click on Manual card on the guard's home page
- Fill in the form with required forms
- Click Grant
- Add an observation and save.
- Go to logbook, on the left menu click on logbook
- Check if the entry has been successfully logged

**1.3. Registered Guest Entry:**
   1. Creating a registered guest
   2. Granting access to a registered guest  
   
**1.3.1 Creating a registered guest**

- Login as an admin in incognito mode 
- Go to logbook and click on invite guest
- Fill in the form with all required fields add a valid date in the community timezone
- Click invite to register the guest
- Check logbook under registered guest tab if the guest has been successfully registered  
    
**1.3.2 Granting access to a registered guest**

- Go to logbook registered guest tab
- Find a user and verify that the time is valid as it indicates
- Click on grant access and add an observation
- Check if that log was successfully created

##### 2. Time Sheet Check in/out Test

- Login as a custodian in incognito mode using: custodian.dgdp@gmail.com
- Find the guard 'mama Guard'.
- Start timer, wait 20 sec, then Stop Timer.
- Verify the entry is present in the Timesheet log.   

##### 3. Payment Test

- Login as an admin on [staging-demo](https://demo-staging.doublegdp.com/)
- Find a client user with a payment plan and take note of the current balance
- Make a payment and note the balance
- Check the receipt
- Revert the payment and note the balance

##### Deployment
Deployment now happens automatically every Monday to Thursday at 10AM CAT. It's expected that engineers verify their tickets in Staging and add the `Staging::Verified` label before this time. You're advised to always do your verification at the end of your day if you think you may not be available the following morning before the auto-deploy kicks off. A slack reminder has also been set up to notify all engineers 30mins to the deployment time. Once you see this notification, kindly check again and ensure that all the necessary tickets are verified and that all verified tickets have the `Staging::Verified` label.

Our auto-deploy script is handled by [Gitlab scheduler](https://gitlab.com/doublegdp/app/-/pipeline_schedules) and this makes it easy to control when the script should run.

- In case of issues, to disable auto-deployment, find the "Push to production" schedule on the [schedules page](https://gitlab.com/doublegdp/app/-/pipeline_schedules), click the edit button, mark it as "inactive" and save.
![Screenshot_2021-10-20_at_12.43.52_PM](/uploads/9e91a706247bc9d8ebd73d186a479841/Screenshot_2021-10-20_at_12.43.52_PM.png)



- In case of emergency, we can also run the script and push to production at any time. We only need to click the "play" button shown in the image below.
![Screenshot_2021-10-20_at_12.44.09_PM](/uploads/39aea58c832c94a39e681849eb4a6d82/Screenshot_2021-10-20_at_12.44.09_PM.png)


**Note: In cases production is down after a deployment, The best thing to do is to roll back to the previous version right away.  Before rolling back, first identify if there are any changes preventing the rollback.**

##### Monitoring Issues In Production

We use Rollbar to alert us of javascript and rails exception that have occurred either on the server or client side


