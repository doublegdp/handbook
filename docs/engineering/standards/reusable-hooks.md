# Reusable Hooks

This is a list of reusable hooks and how they should be used 


### useDebouncedValue
This is a hook that allows us to get a debounced value after the user types something in an input field, we mostly use this for search to avoid constant querying the server for realtime searches. It returns an initial value passed to it, a debounced value and a function that updates that value.     

Usage here: [GuestsView.jsx#L36](https://gitlab.com/doublegdp/app/-/blob/master/app/javascript/src/modules/LogBook/Components/GuestsView.jsx#L36 )    
Source here: [useDebouncedValue.js](https://gitlab.com/doublegdp/app/-/blob/master/app/javascript/src/shared/hooks/useDebouncedValue.js) 

### useFetchMoreRecords
This is used to fetch more records for a given GraphQL query, if more data is found it merges it with current data and returns everything, This hooks returns an object that has a function and a boolean value that checks whether there is still more data to be returned. 
Note: Make sure you use the returned boolean value to disable the load more button so that you don't send unnecessary queries to the server. 

Usage here: [AmenityList.jsx#L28](https://gitlab.com/doublegdp/app/-/blob/master/app/javascript/src/modules/Amenity/Components/AmenityList.jsx#L28)    
Source here: [useFetchMoreRecords.js](https://gitlab.com/doublegdp/app/-/blob/master/app/javascript/src/shared/hooks/useFetchMoreRecords.js)

### useMomentWithLocale
This basically returns a translated instance of momentjs, It makes use of the current community locale and we later use this instance to translate dates according the locale that is supported by that community. 

Usage here: [CommunityNews.jsx#L68](https://gitlab.com/doublegdp/app/-/blob/master/app/javascript/src/modules/Discussions/Components/CommunityNews.jsx#L68)  
Source here: [useMomentWithLocale.js](https://gitlab.com/doublegdp/app/-/blob/master/app/javascript/src/shared/hooks/useMomentWithLocale.js)

### useMutationWrapper
This is a wrapper of Apollo Client's `useMutation()` it takes in a valid GraphQL mutation, a callback function and a callback message, it handles the mutation and what happens after, if it is successful it shows a snackbar alert message, if an error happens then it will show an error.   
It returns an array of 2 items, a function that will trigger the mutation and a loading state to check whether the mutation is done or not. As you can see from examples below you can name these whatever you want, this way you can use the same hook for more than one mutation call in one component. 


Usage here: [AmenityForm.jsx#L21](https://gitlab.com/doublegdp/app/-/blob/master/app/javascript/src/modules/Amenity/Components/AmenityForm.jsx#L21)  
Source here: [useMutationWrapper.js](https://gitlab.com/doublegdp/app/-/blob/master/app/javascript/src/shared/hooks/useMutationWrapper.js)

### useStateIfMounted - Coming soon 

This hook fixes `Can’t perform a React state update on an unmounted component in React-hooks Error` The error reads like `Can't perform a React state update on an unmounted component. This is a no-op, but it indicates a memory leak in your application. To fix, cancel all subscriptions and asynchronous tasks in a useEffect cleanup function`. The cause for this is when within a mutation, we update a state value depending on return data like set an error value or set lodaing status. To solve this error you have run this below command.

You can use it this way: const [count, setCount] = useStateIfMounted(0), const [loadingStatus, setLoadingStatus] = useStateIfMounted(false), const [parentTaskData, setParentTaskData] = useStateIfMounted({}). Basically what the hook does is check if the component is mounted before updating the state. More in the hook logic!

Usage here: [TaskListConfigure.jsx#L18](https://gitlab.com/doublegdp/app/-/blob/master/app/javascript/src/modules/Tasks/TaskLists/Components/TaskListConfigure.jsx#L18)  
Source here: [useStateIfMounted.js](https://gitlab.com/doublegdp/app/-/blob/master/app/javascript/src/shared/hooks/useStateIfMounted.js)
