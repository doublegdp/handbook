## Vision
DoubleGDP succeeds by accelerating growth and delivering responsive public services to our city and real estate partners. We actively strive to push-out relevant content and provide marketing assets that attract prospective city and real estate partners of our platform, and prospective residents to our partner cities. This helps support our mission of accelerating the growth of new great places to live. 

## DoubleGDP Marketing Audiences

### City builders
- Description: Governments, entrepreneurs, and organizations that are building self-stylized new cities of the future. 
- Marketing Style: Typically are curious about master/urban planning, egovernance/digital public services, current economic and legal reform (ie SEZs/Charter cities), methods for acquiring and selling land, equity and debt issuances, and attracting anchor tenants (pioneering residents and businesses). _Data-driven, urbanization-focused, 'wonkish', contrarian, visionary_
- Examples: Blog post on common challenges at differing stages of city development, twitter posts with an urbanization graph
- Pre-DoubleGDP Software Vendors: A suite of software products, not fit for purpose, poorly connected, and not designed for emerging markets
- Goal: Marketing Qualified Leads (MQLs)


### Property/Real-estate Developers
- Description: Residential or multi-use property developers that are building within new cities, or at a large enough scale to be full communities on their own. 
- Marketing Style: Interested in how they can differentiate their real-estate offerings in order to attract buyers/tenants. Are looking to cut costs, shorten timelines, and increase revenue in order to increase their Return on Investment "ROI" and meet/exceed their Internal Rate of Return "IRR" on their initial investment _Industry-relevant, differentiating, return-focused, efficient, profit-maximizing
- Current software vendors: Property Management software not designed for large mixed-use developments nor emerging markets
- Goal: Marketing Qualified Leads (MQLs)


### Pioneer Residents
- Description: Citizens who would be interested in buying land and/or moving into a new community. Knowledge Workers, Job Seekers, Retail Real Estate Investors. We hypothesize that land purchasers are typically upper-middle-class residents of a nearby city or members of the diaspora looking for investment opportunities. Initial residents are either choosing so for job reasons (eg move closer to work) or for consumption reasons (ie moving for a better quality of life). 
- Marketing Style: Interested in investment opportunities and where are new great places to live, work, and play. _Accessible, return-oriented, visionary, amenity-focused, community-centric_
- Example: Blog post on investing in new cities, sharing information about internet speeds & coworking/coliving areas
- Goal: Population of DoubleGDP Supported Cities

## Outbound Marketing
- Account-Based Marketing
- Social Media (Twitter, Facebook, Youtube, Linkedin)
- Targeted ads
- Note that to manage LinkedIn company page, your personal LI account must be added as an admin. There's no separate login via 1Password

## Inbound Marketing
- Website
- Blog Posts
- Overview Deck
- Traditional Earned Media (eg news articles)
- Podcasts: To help reach niche targeted audiences by providing information and knowledge that is interesting to the listener.
