## Main Gate Setup

In order to set up a station that functions and enables the security detail to provide efficient, quick and secure Gate Access to all patrons affiliated with the community, the following needs to be taken into consideration.

### Smartphones and Data Plan

DPDG recommends having at least one smartphone at the Main Gate for the Security Guards to be used to have access to the Community App. Administrators are encouraged to acquire these smartphones with a permanent data plan to ensure that the Security Guards will have permanent access to Internet connection in order to use their Community App. A backup phone is also recommended to be in place in case of failure of one of them. If City Administrators can not acquire these phones, DGDP can provide the phones and back up a data plan.

Based on our experience with other City Partners, DGDP has some preferred smartphone models, which are not strictly the only ones that can be used for this purpose. The phones and data plans recommended are:

1. Samsung A10s
2. Monthly data plan of at least 15GB

DGDP strongly recommends that the Administrators also include a configuration for restrictions on the phone to ensure that the phone will be used exclusively with the purpose of accessing the Community App. **Hexnode** is a well recommended option for this configuration and can be provided by DGDP if City Administrators require it. Instructions on to configure Hexnode can be found [here](https://handbook.doublegdp.com/city-processes/main-gate-setup/#hexnode).

Specific information for Ciudad Morazán phones and data plan can be tracked through this [spreadsheet](https://docs.google.com/spreadsheets/d/172awczqOSM4juCXBVU2zcoxXY0lZTpBMKA2yDQ40zbQ/edit#gid=0).

## Nkwashi

### Toilets

DoubleGDP has provided a mens’ and woman's’ portable toilets near the guard house. They are serviced every two weeks by Sanitech.

### Desks and Chairs

To get all residents, clients and contractors who regularly visit the enrolled onto the DoubleGDP Application it is important to set up a comfortable working space. A simple working space with desk, chairs and adaptor for power plug ins (for example the laptops and printer that prints out ID Cards for clients and residents) is enough to make a productive working environment.

### Smart Phones

For the DoubleGDP application to be used and run effectively a phone is a must-have and in the case of an accidental incident occurring or the phone not responding, a second phone is kept at the station to ensure that there is always a fallback plan.

#### Power
If the gate does not have a readily available power supply, provide a battery pack to keep the phones charged. ([Anker](https://www.amazon.com/Portable-Charger-Anker-PowerCore-20100mAh/dp/B00X5RV14Y) is a good option.) If the battery cannot be charged at the gate, we will need to provide two and define a process for them to be swapped and charged on a regular basis.

### Internet Access

Running the application relies on having constant stable internet access. The phone is currently powered by mobile data to provide all around the clock internet access. Data Bundles are purchased from **MTN and Airtel**.

To be more cost efficient and assured that the Guards phone is always connected to the internet, Mobile Data is prioritised as the Guards phone is always at site and in use. The phone is dual sim card holder and both Airtel and MTN are used for data usage.

The document on this information is here: [https://docs.google.com/document/d/10dGSS7XKxNNiH0J4WfF0L-5ifQVGphU8U0aTaVilar4/edit](https://docs.google.com/document/d/10dGSS7XKxNNiH0J4WfF0L-5ifQVGphU8U0aTaVilar4/edit)

**Data balance** for **Airtel** can be checked by dialing *575# and following the prompts.

**Data balance** for **MTN** can be checked by dialing *777# and following the prompts.

### Set Reminder on Calendar

It is vital that the guards and custodians smart phones are always connected to the internet, set a monthly reminder on your calendar so as not to forget to purchase the data bundles they need to fulfill their work and also to avoid unnecessary use of paper backup (mentioned below) and no recorded information for that particular day(s).

### Paper Backup

In the event that access to the internet is interrupted, a Manual Log is required. This still documents the flow of traffic in and out of the community. Centralised administration of phones been used by guards, installing Hexnode on the phones enables the organization to control the device remotely and restrict data usage. The Manual Log used here; [https://docs.google.com/document/d/1niTNs4Gwl66lMFr25rIdCf-cXf6r1_Y2hz7mIBBdiis/edit](https://docs.google.com/document/d/1niTNs4Gwl66lMFr25rIdCf-cXf6r1_Y2hz7mIBBdiis/edit)


## Hexnode
We use Hexnode on phones provided to security guards by DoubleGDP in order to simplify the interface, allow remote management of the phone, and prevent access to non-DoubleGDP applications that may drain the battery or use up a data plan. It is recommended to add Hexnode to phones after a few iterations of an initial deployment, because the restrictions introduced in Hexnode also limit maneuverability to detect and address issues that may come in the deployment. Once a process has been introduced and is successful, it can be reinforced with Hexnode.

### Hexnode setup
When we do set up Hexnode, here's a high level of our configuration. (This is recreated from memory, and should be elaborated upon the next time we go through the process. The Hexnode support team was instrumental in setting it up the first time and is very responsive.)

1. `Install` the Hexnode app on the phone, and `Enroll` the device. Instructions are [in Hexnode](https://doublegdp.hexnodemdm.com/enrollment/#/platform-specific/android-qr-email-sms/).
1. We have a `Policy` called [`GuardPhone-1.01`](https://doublegdp.hexnodemdm.com/policy/#/my-policies) that has the configuration. You'll need to installer
1. Test to ensure it works.
    - Confirm guards still have access to the app,
    - can connect to the internet,
    - can place and receive calls from within the app. (Not sure if they will have access to phone outside of app)
1. Give the device a name using the `Manage` tab. e.g. "<city>-gate-phone-<#>"
1. Start a `Remote View` to ensure access

**NB:** Our login to the Hexnode interface is available in the `Engineering` vault of OnePassword, and the setup accessed via [https://doublegdp.hexnodemdm.com/](https://doublegdp.hexnodemdm.com/).
