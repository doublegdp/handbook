---
title: Staff In Residence Program
---

## Purpose
The purpose of the Staff in Residence (SIR) program is to provide housing for staff of DoubleGDP, Thebe, Explorer School and Chena Art House. Staff shall volunteer to participate in the program. 


## Roles & Responsibilities

#### DoubleGDP's SIR's Commitment
- Provide housing for a 12 month period. 
- Provide daily transport from Nkwashi to Lusaka City
- On joining the program the SIR participant will receive transport from Lusaka to Nkwashi to move their personal items/furniture. 
- Receive a supermarket voucher when they move to Nkwashi.

