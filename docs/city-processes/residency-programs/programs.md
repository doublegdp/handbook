---
title: Programs
---

DoubleGDP in some cases co-invests in programs with our city partners in order to achieve our goal of population growth. Programs are strategic levers that allow us to coordinate people, resources, and software to achieve growth goals.

In particular, at Nkwashi we partner with Chena to create the [Artists In Residence Program](https://handbook.doublegdp.com/City-Processes/Residency-Programs/air-program/) and [Microverse](https://microverse.org) to create [Hackers In Residence](https://handbook.doublegdp.com/City-Processes/Residency-Programs/hackers-program/).

## Investment Principles

We aim to use program capital to address coordination problems or accelerate growth. We want to be judicious in our spending, and these principles help guide how we approach the budget:

1. Any vendor or partner we work with must be transparent about any fees they charge
1. We pay fair market prices consistent with local rates for all work and materials
1. We aim to avoid perverse incentives
1. When possible, we want to leverage our money in combination with investments from others, which helps create broader support and more stakeholders
1. Our investment should be time-bound -- it helps start a program that will aim for financial independence
1. When we make payments to our city partner in order invest in a program, we expect them not to take a profit margin on those funds
1. We don't pay city employees or contractors

Some examples:

* Pre-paying can be a good accelerant and help the market move faster. For instance, we can pay landscaping costs on a house the program will occupy in exchange for a reduction in rent.
* Avoiding perverse incentives means that we want to encourage behaviors that are economically reasonable but may be out of reach rather than those that are done just because of our investment.

### Paying for City Services

Cities are responsible for providing resident services such as electricity, sewer and water supplies. In cities where DoubleGDP runs residency programs, the customer success managers will inform the city of the required needs to ensure the services are supplied on time.

There may be instances where DoubleGDP may take on the cost in order to accelerate the process. In these instances the CSM will:
1. Share the details with the Head of Customer Success for approval
1. Share the details with city management for acknowledgement and confirmation the costs can be offset through future costs
1. Update the [residency cost spreadsheet ](https://docs.google.com/spreadsheets/d/1B33l-fkFbft-xEjy6UORTyDor6rzovLXT1IVedUtPGk/edit#gid=1069522099)once services or products have been procured


## Residency Programs' Spend

DoubleGDP uses rewards programs to accelerate action and influence behaviours that would have an impact to the growth goals. The budget for 2021 is capped at USD 500,000. Any additional spending is limited to increments of USD 100,000 and a corresponding justification must be provided.

Considering resource constraints and to manage resource allocation, a [business justification](https://gitlab.com/doublegdp/handbook/-/blob/master/.gitlab/issue_templates/Program_Justification_Template.md) (links to Gitlab issue template) will be used to assess the need for the spend. The following guiding questions can be used:

1. What is a brief description of the rewards program?
1. Why are we doing it?
1. Which objective or goal does this program help us achieve?
1. How do we execute the program?
1. When do we get value from the program?
1. How will we measure the program's value?

The spend will be approved by the Head of Customer Success and the CEO.

## Disposal of Acquired Assets

Acquired assets for the duration of the program that may no longer serve purpose or entirely need to be auctioned off can be communicated to the PeopleOps@doublegdp.com who will follow up on the value of the asset and consultation of the accountant.
