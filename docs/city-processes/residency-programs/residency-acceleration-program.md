
## Overview

The Residency Acceleration program is a partnership between DoubleGDP, Nkwashi and its clients. The aim of the program is to encourage Nkwashi clients to start, accelerate or complete their constructions in 2021.

The Nkwashi Client database records over 700 clients with plots fully purchased (more detailed information can be found [here](https://docs.google.com/spreadsheets/d/1ofcWElUjpZsYs-6kJyxbTNeOMO7Vh1gc3DASH__g_68/edit#gid=930362785). This stage of the Residency pipeline goes to show the potential in reaching a goal of 100 Residents by 2021-12-31. The stages following a fully purchased plot (i.e eligible to start construction, Floor Plan purchased, Building Permit approved, construction in progress and construction completed) have much fewer numbers in comparison to plots fully purchased hence DGDP's involvement in the acceleration to next stages.

### Floor Plan Rewards Program

This is targeted at clients that have completed the plot purchase and are yet to buy floor plans. When a client has a floor plan they have made their first steps in the construction process as the floor plan is used to build a bill of quantities and determine the construction budget.

In this program, Nkwashi clients will pay two-thirds value of their preferred floor plan by a defined date and DoubleGDP pays the remaining third. All payments are channeled through Nkwashi City.


### New Constructions

This is targeted at Nkwashi clients that have their bill of quantities and building permits approved and are yet to start construction. The approximate cost of construction for the substructure is $2,5000. Clients that start the construction of the substructure in this program get 75% of the substructure construction cost met by DoubleGDP.


### Constructions in Progress

This is targeted at Nkwashi clients that have active construction projects irrespective of the construction stage. DoubleGDP will contribute up to $5,000 that will go towards construction costs. This excludes Atlas Mara and Lafarge projects.

### What criteria do we look for to make payment to client?

1. Eligible to start construction with approval from Nkwashi
1. Commitment to financing their percentage of the current construction stage
1. A signed contract between client and contractor
1. A signed contract between client and DGDP
1. A fully itemized invoice stating what DGDP’s contribution is going towards.

### What constraints / limitations / caveats will we respect?

It is required that all DGDP stakeholders demonstrate full commitment and participation in all processes during the tenure of the agreement signed by the Client. If any stakeholder does not agree with this, all processions will be halted until all parties come to a concerted agreement.

### Closing off the Program

When a Client participates in the program and completes the program by maximizing the DGDP contribution, the next steps are that we close off their participation. To do this, an official meeting will be held between Client, Contractor, DGDP and Nkwashi. To be covered in these handover meetings are;

1. Checklists against the initial Bill of Quantities presented at the beginning of the program
1. How it all went. (Overall experience for all stakeholders)
1. What worked well
1. What did not work well
1. How stakeholders can do better

A follow up email is sent out to all stakeholders to document the end of the agreement.
