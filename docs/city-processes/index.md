---
title: City Processes Overview
---

## Purpose
The City Processes section of our Handbook collects the practices, operations, and training material that are used in conjunction with the DoubleGDP platform in order to run a city.
