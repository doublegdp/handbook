---
title: Customer Related Documentation
---

All Customer contracts signed for the purpose of advancing city operations should be saved in [this folder](https://drive.google.com/drive/folders/1i5uNtiHrxE6JQxX41WGFpTBK4_bFAXDy?usp=sharing)


## Video stream

DoubleGDP has the capability to share a periodic "live stream" through the application, for instance to showcase construction progress. Its current set up takes a 30-second loop every N days and adds it to a news post.

### Camera setup

To setup a construction live streaming is necessary to acquire the right equipment. Here is a list of the basic equipment needed:

1. WiFi - Wireless Camera, like the [Reolink WiFi](https://www.amazon.com/Reolink-Outdoor-Security-Camera-Dual/dp/B08PYN7TS2/ref=sr_1_4?crid=1IVH80TVWSX0E&keywords=wifi+reolink&qid=1659065256&sprefix=wifi+reolink%2Caps%2C109&sr=8-4) is recommended for most of the cases. But if power source is not available there are other options as a Wifi Solar Powered Camera like [Reolink Argus Eco](https://www.amazon.com/Outdoor-Security-Camera-System-Reolink/dp/B08X6CR545) that might be an alternative.
2. For those cases where no power source or WiFi is available a camera like the [Reolink 4G](https://www.amazon.com/dp/B098DY1FHH/ref=sspa_dk_detail_0?psc=1&pd_rd_i=B098DY1FHH&pd_rd_w=bIZlj&content-id=amzn1.sym.3481f441-61ac-4028-9c1a-7f9ce8ec50c5&pf_rd_p=3481f441-61ac-4028-9c1a-7f9ce8ec50c5&pf_rd_r=RD0F7D5EFHT5C96RVYAD&pd_rd_wg=4r7ff&pd_rd_r=5bcbaebc-66a9-4ea1-9b35-c6ddb7dab200&s=photo&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUFIVFRCQlJJWUFDNTUmZW5jcnlwdGVkSWQ9QTEwMjg1MTdPVU5WRUZSNElZVVkmZW5jcnlwdGVkQWRJZD1BMDA3ODM0MDMzTThOVEFCVE1IMzEmd2lkZ2V0TmFtZT1zcF9kZXRhaWxfdGhlbWF0aWMmYWN0aW9uPWNsaWNrUmVkaXJlY3QmZG9Ob3RMb2dDbGljaz10cnVl) can be an option. This type on camera will allow City Admins to set up the camera and don't worry about energy resources or connectivity options.
3. If you purchased a WiFi Camera and there's no WiFi  available within camera range, a Mifi device with an option for solar power charging system is recommended.

It is highly recommended to acquire a camera with an option to access the device remotely in order to gather video recordings.

### Maintenance

- If the camera that was installed for live stream a community site was a 4G LTE device and requires a data plan to work, this should be discussed with City Admins to make sure that there will be sufficient data to live stream for a period of time. 5GB of data should be sufficient for a HD quality recording on a 10 minutes weekly basis.
- Battery lifetime should be noted and considered when installing a live stream camera to garantee that when the battery lifetime is reached, there should a replacement.
