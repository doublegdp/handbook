## Security

### Security Policies
1. All visitors who access the Nkwashi site must sign in with the guard on duty. This can be done by scanning a QR code or completing the digital entry form on the phone. This ensures Nkwashi has a record of all site visitors. Either of these methods will provide immediate notification to security administrator(s) so they can monitor activity for any concerns that guards may not notice.

        A. If the internet is not functioning properly or a known client objects to using a digital log, or if, guards may fall back to a paper log. A DoubleGDP CSM will pick up these forms at the        end of each week and manually enter these. We aim to minimize this usage, since it introduces a long time delay.

        B. Security administrators, listed below, have authority to allow exceptions to this policy. 

2. Clients may visit the site during normal visiting hours; Residents may visit at all times. These hours are determined by the primary security administrator, shown below.

3. Security discs on a car indicate that the person is a client and should be granted access. They still should sign in with the process above, and once they complete the log should be given access to the site.

        A. NOTE: we would like clarification on the meaning of “perpetual access,” which is how clients are introduced to discs today. Is this consistent with policy of “visiting hours” that we’ve heard from Poniso?

4. Guards must take and record the temperature for everyone entering the site

### COVID-19 Precautions
1. The main gate should be provided with the following supplies

        A. Disposable masks

        B. Nitrile gloves

        C. Hand sanitizer

        D. Disinfecting wipes

        E. Liability release waivers and a clipboard

        F. 50 to 100 pens

        G. Two baskets for pens -- one labeled “clean” and one labeled “used”

2. Guards should follow these procedures: 

        A. Wear masks and gloves whenever a visitor approaches

        B. Sanitize their hands after any visitor leaves, if they handed the phone to the visitor or made physical contact

        C. Wipe the phone at the beginning and end of each shift

        D. Offer a mask to any visitor not wearing one

3. All visitors are required to sign a liability waiver. Guards should follow this process:

        A. Welcome to Nkwashi and explain that we’re taking precautions regarding covid-19

        B. Explain that liability release is required

        C. Provide a pen from the “clean” basket

        D. Hold the clipboard while visitor signs (rather than hand it to them)

        E. Place the pen in the “used” basket

        F. Periodically wipe the pens in the “used” basket and put them in “clean” 

4. Picnic procedures (to be modified by 

        A. Follow guidelines from US CDC: https://www.cdc.gov/coronavirus/2019-ncov/community/large-events/considerations-for-events-gatherings.html
        
        B. In addition to those, 

### FAQs

We can use this section to handle exceptions to the process above.

1. How do we handle construction trucks?

2. How do we handle special events when lots of people may come at once?

### Security Administrators

These are the people who have authority to allow unrecognized people onto site:

1. Poniso

2. Pezo

3. Tamara

4. Sepo

5. Mwiya

### App Administrators

Nkwashi and DoubleGDP employees are administrators of the app and have access to enroll new clients and set their default privileges. 

Guards have access in the app to scan visitors, look up users to see their status, review entry logs, check in users at the gate.

