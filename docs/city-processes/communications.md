## Communications

### Communications

DoubleGDP provides a weekly status update to keep Nkwashi administrators informed of client activity, issues, and what’s being worked on. The app operates on a sprint cycle that means new features roll out every two weeks. This section outlines how we will handle communications in order to stay coordinated around changes in policies or in the app.

### Clients 

All clients should receive an SMS introducing them to the app and notifying about security policies. We also have the following signage put up at Nkwashi Estate Gate: [Main Gate Poster](https://docs.google.com/document/d/1Mujqn3XJ9uI-pe1QgBuLAxrGUzynkUrWYlqkaCp1LWg/edit)

App administrators may enroll new clients. This is done by sending a link via SMS that will enroll and log them in, and expires after some time so that clients do not forward it to others.

New features rolled out to clients can be communicated directly in the app; those that require more awareness or that warrant promotion will be coordinated with the marketing team.

### Guards

DoubleGDP trains guards on the policies listed in this document and on usage of the Nkwashi app. Small changes will be communicated ad hoc. Introduction of major features or significant changes in policy will be rolled out with a formal training to ensure understanding and allow questions. 

### Sales Staff

In order to coordinate well and properly set client expectations, we recommend coordinating training updates with the sales team on the same intervals as with guards.

### Feedback

After a registered user checks in at the gate they receive an SMS asking if they’d like to give feedback. Those results are available for administrators to see at https://app.doublegdp.com/feedbacks

### Support

We strive to deliver great client support and be responsive to feedback. We need to discuss support and escalation processes to help achieve this.

