## Content Marketing

The Nkwashi App currently has a high dependency on content that is required to stay relevant to clients (and Nkwashi) and increase user activity. Each week, content creators are to have 2-4 pieces ready to be strategically sent out to all user types (clients, residents, prospects and visitors) Pieces should be tailored to each specific user type and can be sent out on different days to different groups so as to avoid becoming spammy with the outbound messages. 

The content production process is in different stages.

* DGDP and Nkwashi content creators brainstorm ideas biweekly and decide on which ones are viable for the Nkwashi Digest Newsletter. 
* The content creators then work on the technicalities of the idea so as to bring it to life. 
* The articles are then published in wordpress.
* The final product/campaign link is sent to the Nkwashi marketing manager for final approval.
* Content creators must then create a list of users to send the campaign to and write up the SMS and/or email copy. 
* When that is done, the campaign is scheduled in the application in the campaigns section of the app and sent out to clients/prospective clients. 

Content includes different aspects of Nkwashi, such as life on site, construction updates, home construction testimonials, promotions, etc, and can be in the form of interviews or articles. Bearing in mind that people have less time to read through words and words of content, visuals are an important part of the process. 
 
**NOTE:** In the case that a promotions campaign will be run by Nkwashi, the content production and approval process differs slightly.
 
* The credit management department will communicate the campaign details, create the recipients list and write the SMS copy to be sent out. 
* Content creators must run the campaign by the marketing manager for approval.
* Once approved the campaign is scheduled to be sent out. 


