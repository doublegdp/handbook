## Training the Customer

### Overview

As we continue with iteration of the product and how it has evolved overtime, we have shifted into focusing major effort into user adoption. This includes training our customers to use the application with little to no help. This training is scheduled with the customer by the Customer Success Team and is carried out over a series of meetings.

Training of the customer on the usage of the Application can be conducted either in person or via a zoom call. For the training to be effective, stakeholders will need a laptop, administrator access to the application and fast internet.

### New Features

Customer Success conducts the training of the customer on how to use the application and collects feedback or suggestions which is shared with the engineering team to be worked on. The customer success team also follows ups with the customer to monitor how they are using the features, if it is beneficial to them and if they have more feedback to provide.

### New Recruits

New recruits added to the customer's team should initially be trained by the customer on how to use the application but in the instance that the Customer is pressed for time or lack of availability they can request the customer success team to conduct training. 
