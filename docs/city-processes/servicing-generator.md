## Prefab Village Generator Servicing
 
The prefabricated housing at Nkwashi is currently off grid. For this reason, DGDP has a 20KVA generator on site that is used to power the homes and runs on a schedule. Due to the increasing number of homes and more strenuous use of the generator power, it needs to be serviced biweekly. 

