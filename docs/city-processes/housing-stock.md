# Housing Stock

Housing Stock refers to the total number of dwellings (houses, flats/apartments and prefabricated units) that are available for occupation in the city. Properties forming part of the housing stock can either be occupied or vacant.

## Construction, by City Residents

This source of housing stock is driven by city residents who build units for their individual use or for lease.

The steps to increasing housing stock are:

1. Fully purchased or leased plot
1. Eligibility to start construction
1. Purchased floor plan
1. Approved building permits
1. Active Construction
1. Completed Construction

## Construction, by Developers

Property developers can work with city administrators to provide multiple housing units for sale or lease. 

## Temporary Housing

### Prefabricated Housing

This checklist guides the process for prefabricated housing installation:

1. Identify need that will be met by using prefabricated housing (prefab). Examples include housing for residency program; temporary offices etc
1. Plan for the number of units required; their configuration and the purchase/ lease option
1. Seek and gain approval from the partner city
1. Partner city identifies a location
1. Prefab service provider carries out a site-visit to the location to confirm suitability
1. Confirm availability of services (electricity, water and sanitation) required for the prefab units and plan for their delivery
1. Contract the prefabricated housing service provider
1. Prior to delivery the prefab carries out a site visit to verify that site is prepared to expectations
1. Prefab is delivered and connected to the services
1. Prefab is ready for occupation
