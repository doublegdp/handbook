## Policies

### Site Labor Time Tracking

Starting in May 2020, all site employees will begin registering the starts and ends of their shifts via the app with the stores custodian. Here is the procedure:

    1. Stores custodian(s) will have permissions in the app to start and end shifts.

    2. All site labor employees will have a digital ID through the app. They may access through their smartphone if they have one, and will be provided a physical ID card with QR code if they prefer.

    3. Employees will begin and end each shift by checking in with the custodian.

    4. An administrator will review reports weekly and handle any data corrections or exceptions that may be necessary.

    5. An administrator (with DoubleGDP support to refresh the data) will export the data monthly and load into the employee payroll system.

### Gift Policy

In some cases gifts may be used to apologize to a client who had a poor experience, thank a client who helped, or as an incentive reward for team members performing well.

A “Gift” is anything of value that is given to certain persons outlined in this Gift Policy.
This Gift Policy applies to the giving of Gifts by DoubleGDP employees. Any such person may give Gifts (defined below), either directly or indirectly, only in compliance with this Gift Policy.

DoubleGDP sets specific limits on the types and value of Gifts that may be accepted or given and requires visibility and disclosure of Gifts regardless of type or value as described below.

    * Before providing the client with a gift you must first obtain approval from the CEO in writing (email) or phone call.

    *  Communication of the gift to the client should be made by sms, phone call or email and Gifts given remotely (not in the CEO’s visibility) require a picture taken of the giving of the Gift.

    * All Gifts given should be recorded.

    * We will not give Inappropriate Gifts such as cash, favors, entertainment that would be illegal or violate any law, regulation, or any DoubleGDP policy, including, but not limited to, bribes.

A gift can be provided to the client if they are not satisfied with the service they were provided (it is documented) or have been very patient with the setup process of the application.
This frustration was evident with some residents as they had to be manually logged in but since being enrolled onto the application they have appreciated the fact that they are being scanned in which is faster and easier.

Items gifted to clients can be;

    1. DoubleGDP Swag.

    2. Gift Vouchers to Clothing or Grocery stores.

The fair value for gifts would be K300 with the exception of DoubleGDP Swag which can be given out as a package or only one item at a time. The CSM has maximum pre-approval for gifts/rewards and some exceptions may require the CEO’s approval.

### Incentives for Guards
A reward for accomplishment can be given to the guards to reach a goal or be consistent with the use of the application and discourage the use of the paper log.

    1. Minutes are a first choice for the guards. This could be K100 monthly minutes.
    2. A Bemba Bible was selected by one guard.

**Note:** One guard is on duty 5 days out of the week and there is not much traffic flow in the evening so incentive may be given on the premise that there was;

    1. No use of the paper log for that particular week.
    2. Timely reporting if Routers are not responding.
    3. Problem with the phone/application.
