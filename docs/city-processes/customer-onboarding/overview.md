## Onboarding DoubleGDP City Partners

Through the onboarding process city leaders and staff are introduced to the DoubleGDP product and practices. The process helps users get started, stay engaged and eventually use the app as part of their day to day activities.


### Onboarding Checklist
The onboarding checklist is managed in an epic. Use the [new_customer_onbarding template](https://gitlab.com/doublegdp/handbook/-/blob/master/.gitlab/issue_templates/New_Customer_Onboarding.md) in gitlab to create an issue and later promote this to an epic.

Add issues to the epic for the following tasks:

#### Engineering

-  Create app URL
-  Add google analytics to city app
-  App landing page customization
-  Add wordpress to city app
-  Add city community
-  Setup modules for city
-  Setup branded emails for city
-  Set locales for the app


#### Customer Success

-  Create Success Plan
-  User Welcome Email

### Feature Planning
When on boarding a new customer, we must identify the solution they need and not the solution they want.  Sometimes, customers want a solution they really need and that's great because it makes our job easier.  To identify a solution that will solve the customer's problems, we identify the customer's real problems by observing the stakeholders interact with their existing process and by following the next steps:

- preliminary discovery

  - Overall description of the problem.
  - existing process overview
  - Who are the Stakeholders
  - Expectations from customers

- Detailed discovery

  - Schedule introduction meetings with stakeholders.
  - Conduct in-person meetings with stakeholders
  - Formulate detailed problems among stakeholders.
