## **Onboarding City Staff Overview**

As the number of city partnerships grow and the cities themselves grow, there’s the need to have an onboarding process for key staff members in the cities, so they can manage their processes through these guidelines.

These are additional guidelines to those already provided in [Training the Customer](https://handbook.doublegdp.com/City-Processes/training-the-customer/#training-the-customer) and can be considered as complementary to it.

## **Onboarding a Marketing Lead**

A city Marketing Lead, is an important element for our continuous collaboration within both parties, DGDP and the city. For that, this person should have an active role in the campaigns, social media and the marketing features that the app can provide. Some of the responsibilities and approaches that a Marketing Lead will have, are:



1. Know the tools: here is a list of the most important tools that a Marketing Lead should use and know:
    1. DGDP City app: [Nkwashi](https://app.doublegdp.com/login) / [Ciudad Morazán](https://morazancity.doublegdp.com/login). If you don't have a user and access, ask a DGDP CSM for it. Once logged in, pay special attention to:
        1. CRM (and labels): To understand your customers and build customer persona for marketing purposes.
        1. Campaign management (Email & SMS): To be used for targeted campaigns.
        1. Customer Journey: To be used for marketing and sales funnels.
        1. Newsletters: To be used for broadcast messages to the community.
    1. Wordpress:
        1. To be able to create posts that will appear in the app and be featured on the newsletter (this might include using bitly links and adding UTM).
    1. Canva:
        1. Canva is a user-friendly app that enables marketers with no design background to create engaging content for social media platforms such as Facebook and Instagram.
        1. Getting a “Pro” account is highly recommended as it enables users to have access to thousands of templates.
    1. Instagram/Facebook:
        1. Instagram can be a powerful social media as it is the most visual.
        1. Basic guidelines: posting 3 times a week on feed would be appropriate. 1 post can be about construction updates about residents in the city, another post can be about the residents (a resident profile, “meet the residents”), and a third post can be about lifestyle (i.e. events happening at the city).
        1. Paid ads can be used to generate leads.
    1. Typeform:
        1. Typeform is a go-to tool for surveys. As a marketing lead, getting to know your audience will involve developing a feedback tool. Typeform offers many templates and is user-friendly (for both the survey creator, and the respondents).
    1. DGDP Google analytics.
1. For content pieces for outbound messages, the Marketing Lead should be assisted by a content creator so at least 2 - 4 pieces are sent out strategically to all user types (clients, residents, prospects and visitors). Pieces should be tailored to each specific user type and can be sent out on different days to different groups so to avoid spammy with the outbound messages.

    Content includes aspects of the City, such as life on site, construction updates, home building testimonials, promotions, etc., and can be in the form of interviews or articles, Bearing in mind that people have less time to read through words and words of content, visuals are an important part of the process.


    If the City Marketing Lead has difficulties to accomplish the content for outbound messages, campaigns, or the articles for news, DGDP team can advise and collaborate in the process. This collaboration will involve the DGDP team in the creating process and the final review and approvals will be responsibilities of the Marketing Lead. [These guidelines](https://handbook.doublegdp.com/City-Processes/content-marketing/#content-marketing) can be used as reference if this is the case.   

1. Know the programs that are taking place in your city. This is important as the Marketing Lead is a key stakeholder in the process of making these strategic investments part of the city's growth and reputation. The current programs are:
    1. Nkwashi: Hackers In Residence Program ([HIR](https://handbook.doublegdp.com/City-Processes/Residency-Programs/hackers-program/#purpose)), Artist in Residence Program ([AIR](https://handbook.doublegdp.com/City-Processes/Residency-Programs/air-program/#artists-in-residence-program-at-nkwashi)), Residency Acceleration Program ([RAP](https://handbook.doublegdp.com/City-Processes/Residency-Programs/Residency-Acceleration-Program/#overview)).
    1. Ciudad Morazán: TBD.
