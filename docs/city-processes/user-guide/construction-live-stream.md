## Overview
The Construction Live Stream is a feature that helps our Partner Communities to connect as a sales pitch with those clients / investors that live abroad and want or need to see how the construction of their house/building is evolving.   

For requirements on how to setup the Construction Live Stream go to [Camera setup](https://handbook.doublegdp.com/city-processes/customer-related-documentation/#camera-setup) in City Processes.

### Live Streaming in Community App
After the equipment is purchased, the camera and the rest of the equipment, need to be mounted in the construction site. Once the camera has been tested and Live Streaming is working according to the brand parameters these steps are needed to post the videos in the Community App:

1. Login as an Administrator in the Community Wordpress account. Credentials are available in 1Password shared vault.
2. On Post section, Select "Add New".
3. Complete all the information you want to share with your Live Stream Post Video.
4. Once you have completed the information, click on the "+" button and select "Video".
5. Upload the video you want to share.
6. Publish the Article as you would do with any other News Post.
7. Update the Article with a new updated video according to the time managed by the Admins requirements.

It is recommended to gather a series of the live stream videos and convert them into a single video using a Video editing tool.
