## Admin Resident Registry / Digital ID generation

<div style="position: relative; padding-bottom: 62.5%; height: 0;"><iframe src="https://www.loom.com/embed/e8c34ed4437b4d4aac3b48c0ee1a84ed" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

1.  Admin logs into profile on [https://morazancity.doublegdp.com/](https://morazancity.doublegdp.com/)
2.  Select search
3.  Search for user
4.  If user does not exist select “Create a New Request”
![](https://lh4.googleusercontent.com/mAL4QQrOdXGiPC6kQJ8jREmnxQ-cKb22yUZnF0wMtftDT56Gx0WxjtR5yU-ZyAP7ks5upHCWvqzucFRVuzTMOMkJQ2Bq146vInAM86S_TW3h3tD8XhaCWZPcGI9mR8c-xZGIdJYp)
5.  Fill out the user form (* indicate required fields)
5.1. Full Name*
5.2. Primary Phone Number*
5.3. Primary Email Address*
5.4. External Reference ID (e.g. national ID card or license no.)
5.5. User Type (Admin, Client, Resident, Visitor, Contractor, etc)
5.6. State* (Ensure Valid if want to allow for QR code entry)
5.7. Expiration Date (When should the state expire)
    

![](https://lh6.googleusercontent.com/F-rzn6xZ4y8eMLYVgE-i1hicTJWc5wlLMonlLhTFXaW-4koPyjxE3Cs3jT-47TYs0xB1JKqrRPyL26mrJAkVK9DReYgzZfZoYxazDktUyH5n4ZGJEZ77gtyCZsMIN7eGSOCBS97n)

6.  Select Submit
    
7.  Ensure account is created by select search and searching for user
    
8.  Send one time link to profile by selecting user and **send OTP**

## Guard Digital Entry Flow

1.  Open chrome, enter [https://morazancity.doublegdp.com/](https://morazancity.doublegdp.com/), and login using provided login information.
    

![](https://lh4.googleusercontent.com/M7qL80wxY3ScSnWcni0vypqZp76ESiZMQKR27yHmvSUGq9AnB1RgrU2TxgBPkTKYhBB4f2h3rU2wyY0VF8MRsZpj-48W1qIec-eBZHQIMeIVLYE0scfweF2KGhzj2Ul0uHnl7BBL)

2.  If phone number is used then enter phone number and the one time code received on text.
    

Note: Guards can switch accounts by selecting between drop downs as well and app can be saved to shared phone’s home screen

![](https://lh6.googleusercontent.com/7v5ToLUd8Z4R_aQUlCn6Tnie-3cMxtV_sYMd48FNdcjRqm5l4uNERm6rhF1qc8FuN75JaRUZ4A2fogCiKE8q3L40BNfrkm6YMC5Az8_p9gM4ZL8FxSR8dk8fwQhjYxHBp9AxPfzw)  ![](https://lh3.googleusercontent.com/VsdGWUbWtUB45uJJHcFNDJUoJD_8-aR5WxEoCxtmsbFJA1boT7JjzLczhOLc6gzL6TbBkvDXnZg1_6PaeSCs7oIjGm2ypfBIS4FgBHh5SGax2FlYzdBeE1dJf_nYibABG1szDqka)![](https://lh4.googleusercontent.com/CxgS0wYUatKBv_mnNjnbfwU_uA9UgH-vQcjrRD_GvoW-q9gOIKUWcKd3rUa3VkUo2F52IBLsxrtSP5eNrE9W0a7dxsq_9XhLboVm0gsoP_iVe6NQ0VM6T4EL6Q2FF3RqIdOMBYUu)

3.  Once a visitor approaches, greet them warmly and ask the visitor if they have a Digital ID or QR code available.
    
4.  **If visitor has code**, ask them to present the QR code. **Select  Scan** on main home screen or on search bar. The **phone camera will load**.
    
5.  **Point scanner to QR code** and scan
    

![](https://lh5.googleusercontent.com/ZSvl7ZJTr-J__O2a7Q8gvFcUuviZt6J-jJzLk7arPiUqmGudAl6tSUCvMFmScUlgOf6tI6tb2a3DRC-qavJpV1eYr0QG8wB6rHO0eePWQtoGPg9PRzZd7TxRhFTPUnE9SBh8iqoS)

6.  User profile will load on scanning
    
7.  **If visitor does not have a code**, ask if they have an account. **Search user’s name** through the search bar on home. If visitor found, **select user profile**.
    
8.  Select **log this entry** if account is valid
    
9.  If account is not valid the user will have to be logged in manually
    
10.  Possible states and how to handle:

1.  	Valid: Grant Access
    
2.  	Pending: Inform to speak with admins
    
3.  	Not Allowed: Inform Head of Security
    
4.  	Expired: Inform Head of Security

## Guard Manual Entry Flow

1.  If visitor does not have an account or QR code then select Manual
    
2.  Enter Name, Phone Number, ID Number, and Vehicle Plate Number if needed.
    
3.  Provide reason for visit
    
4.  Select Submit
    

![](https://lh5.googleusercontent.com/fwoD-rtbZUQsjibmjFzQXHss_AC58M-2heSAgsJYclujmeXkGXf6UaFb8kWjDsMmikXec-Crp0j1MojF2MXO6KZg7LBNBCDddYTSee27vt3eEZzYY8YahlBKsmmYdZcupzvR5dtL)

5.  Grant or Deny access and then allow entry into site

## Admin and Guards view new and previous visits

1.  **Select logbook** on left hand menu to view entries
    
2.  **View list of all entries** to site and view individual entry through More Details. Manual indicates manual entry. Digital scan indicates QR code scan. Print indicates search or printed ID scan
    

![](https://lh4.googleusercontent.com/lGgN9jdMC7yXs3f4EC6iq_fpGEZ_a1OZITUom-jjs0BOVkH5xzspSOMzNnGe5PQ2xj4cFdKb85NcikxncRGC2Ug_JjdEtVxfWNOrsZndVFkRypH05vL3AXBlFHOO-LdI2MDgicpj)

3.  **Use filter bar to search** for specific entries
    
4.  **New visits tab shows first time visitors** to site
    
5.  **Upcoming visits shows visitors pre-approved by admins** to be on site

## Admin submit visit request

1.  **Select logbook** on left hand menu to view entries
    
2.  **Select New visit request** on bottom right of screen
    
3.  **Fill out form** with Name, Phone Number, Reason for visit, and Time
    

![](https://lh6.googleusercontent.com/_fqThCJyG5j5Dp5kSNGd0IxoaktlpNGYAmgC5whKDgPIUuhgRjAi1sbWW-Vq2b4mW7nancvZIq571eT14V3WqOKRfxCC3hzrH_okDxcEdDezf9pa1fcDyD6sORfzI0LOn0fHhYa_)

4.  Select Submit
    
5.  Visit request is available in logbook under Upcoming Visits
    

![](https://lh6.googleusercontent.com/up3J76iUmsxdI00XT0iAwseCSgHEv1vSkeUPiMJUVpTnB9lBGBa1uTKc7ztB1GK3PTk3eTHx8LKGUwXE6FKJ2Htgk5eC34yVt6SzM8--f5HS0_TGcKT-aMrBVhpIZ_S0vDh4eJ-H)

  
  
  

Spanish version available here: [https://docs.google.com/document/d/1Wgz_12aMSwhHL_Gdknn4NucREk-r8EUDfFpIWl7-lTY/edit?usp=sharing](https://docs.google.com/document/d/1Wgz_12aMSwhHL_Gdknn4NucREk-r8EUDfFpIWl7-lTY/edit?usp=sharing)

## Generating (downloading) QR Codes

Every user can download its own QR Code to present it at the entrance of the city. As an Admin you can also download the QR Code for someone else. Here are the steps to generate (download) QR Codes in both ways.

**Downloading Personal QR Code:**



1. Login with your account to the City App.
2. On the left side of your screen, go to the main menu (three bars).
3. Select “My Profile”.
4. On the secondary menu on the right side of your screen, select “Print ID”.
5. The QR Code will be displayed.
6. Press “Download ID”. The QR Code will be downloaded onto your device.
7. Search for the QR Code in your device and print it if needed.

**Downloading QR Code for someone else (as an Admin):**



1. Login with your account to the City App.
2. On the left side of your screen, go to the main menu (three bars).
3. Select “Users”.
4. In the search bar, type the name of the user that you're looking for.
5. Once you’ve found the user, click on the user. The user information will be displayed.
6. On the secondary menu on the right side of your screen, select “Print ID”.
7. The QR Code will be displayed.
8. Press “Download ID”. The QR Code will be downloaded onto your device.
9. Search for the QR Code in your device and print it if needed.
