## SMS Campaigns

To send out campaigns to clients, you’ll need to have created profiles in the App for that particular group.

1. Go to users
2. Filter as desired by either role, label, phone number, sub status or range for date of login.
3. Click the filter icon again
4. Select “All”

You now have two options. To either send a campaign or assign a label. For the latter, you can do this in the case that you want to assign a new/other label to the selected profiles. To proceed to  creating a campaign;

5. Click the "create a campaign" option
6. Fill out the fields as desired
7. Ensure its set to “scheduled” and not “draft”

![schedule vs draft in app](/img/draft-schedule.png)

8. Click update campaign

# Calculating Campaign Statistics

Total Sent for SMS and Email campaigns are found using the response we get from 3rd party APIs which we are using to send SMS and emails.
1. Total Success % is calculated using (Total Sent / Total Scheduled) * 100.
