## How to Begin a Discussion

The purpose of this feature is to actively engage the community/app users. To create a discussion, 

1. Go to "Discussions" under "Community".
2. Click "Create a Discussion Topic" at the bottom right of your screen. 
3. Fill out the the Discussion title and description.
4. Select "Submit".

To make the Community aware of the discussion created, admins can send out campaigns highlighting the particular board or include a hyperlink in the Call-to-Action section of an article. 
