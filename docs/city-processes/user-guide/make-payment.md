## Making a Payment

#### How Payment Works

We are currently using [FlutterWave](https://flutterwave.com/) to process payments in our app, This means that for a community that has Transactions enabled can receive payments from their users.  
If a user needs to make payment for something e.g: a land, or registration fees. They can go to [https://demo.doublegdp.com/payments/pay](https://demo.doublegdp.com/payments/pay) fill in the details of their payments, for this we require the following information to be submitted:  

- Account Name - For the person who is making the payment
- Invoice Number - This is not generated from our app
- Amount - How much are they paying
- Description - What they are paying for or anything else

Once all this information is provided, a user can pay using a payment method that is supported by their community and region, We keep a log of all payments users make and this information is also available in flutterwave dashboard.

#### What's needed to set up payments

Each community should have a flutterwave account where the money paid by users will be redirected to, DoubleGDP does not handle the money paid, this goes to the client's account directly we only keep a log of who paid and how much they paid and all other meta data about the payment.

For users to be able to make the payments we need the following information from a community:  
- Public API Key
- Secret API Key
- Encryption Key

The above information can be obtained by following the steps below

1. Sign Up for flutterwave and complete their KYC(Know Your Customer) process
2. Once the process in step 1 has been completed, go to Settings, The last menu item on the left side
3. After that click on the API tab and share those only with the DoubleGDP engineers via secure channels

Once we get these, we will update them to your community and you will be good to receive payments from the users.
