## Recording a Payment

To record a payment, you need to make sure as an Admin that the user you wish to record a payment for has a profile on the App.

Search for the User then click on the menu on the right side and select Payments.

Click <a href="https://www.loom.com/share/cad8a10f388b44ff88639ac4e6b908da">here</a> to learn how to record a payment.
