## Creating an Email Template

1. Go to https://app.doublegdp.com/mail_templates
1. Click the +Create button at the bottom of the screen.
1. On the canvas, you can now begin creating your newsletter. Drag items from the right panel onto the canvas.

![Email template outlook](/img/email-template.png)

1. Once you're done, click save at the top right of the screen.
1. Name your template.

This template will be used to send out a targeted email campaign.

## Setting up an Email Campaign

Sending bulk emails to users can be achieved in a similar manner as sending bulk SMSes. The difference is that as an administrator, you should have already created the Email template in the App. To do that;

1. Go to [Email Templates](https://app.doublegdp.com/mail_templates) under "Community".
1. Click "Create" at the bottom right.
1. Design template as desired.
1. Click "Save" and name the template.

The next step is where the SMS and Email Campaign processes sync. To create the campaign, view the process [here](../creating-sms-campaigns/).

### Note
- Be sure to select the Email template created earlier in the "Campaign type" (the first field on the [Campaign page](https://app.doublegdp.com/campaign-create)).
