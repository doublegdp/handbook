## Fixed Assets Tracking & Management


*   Fixed assets such as office furniture, computer equipment, laptops, are purchased for long-term use. They usually have a useful life of more than one year. DoubleGDP tracks its fixed assets with a unit price over $500 USD via this document [Employee Equipment - Fixed Asset Tracking](https://docs.google.com/spreadsheets/d/1QCesUFmXBiuTcgf2UI5jnc7M4QjqwpcoMhw8rXaGgGs/edit?ts=5fab0d81#gid=0).
*   The purchaser of the assets is required to log in to this file to fill out the information (as indicated in the file), such as date of purchase, purchaser's name, the value of the asset, etc.
*   Any team members who received the company purchased laptops or computer equipment are required to log in to this file and fill out the asset description, model #, and serial #.
*   Please make sure the information of the Fixed Asset Tracking file is completed within 7 business days of the purchase date when the asset is initially purchased.
*   If a transition happens from existing computer equipment to another team member, the recipient of the computer equipment is required to acknowledge the receipt of the computer equipment within 7 business days. Please log in to the Fixed Asset Tracking file and fill out the required information (as indicated in the file), such as the description of the asset transition, date of transition, old owner, new owner, etc.
*   **Purchasing Company Assets that are no longer used for Business**

    If an employee is interested in buying a company asset that is no longer used for business, the CEO will review case by case, with consideration to whether an asset can be used again, value to the company, and define a fair market value for sell. CEO should notify Accounting immediately if a decision is made to allow an employee to purchase the company's assets, so Accounting can make sure this is recorded properly.




