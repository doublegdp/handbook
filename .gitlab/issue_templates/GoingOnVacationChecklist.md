### Going on Vacation or are taking time-off?

The following is a checklist that you can you use to help you manage a smooth transition and for you to having a quiet time off while ensuring the company functions adequately.

 - [ ] Reserve your time using PTO by Roots.
 - [ ] Find someone who is able to attend your meetings.
   - [ ] Transfer ownership of meetings if necessary
 - [ ] Cancel your remaining meetings.
 - [ ] If the time off falls at the end of sprint, find someone to record your sprint video.

