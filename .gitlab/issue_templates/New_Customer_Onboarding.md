Use this template for initial onboarding of DoubleGDP parter cities. 

**IMPORTANT:** Once the template has been created, promote it to an epic. 


Partnerships:

* [ ] Signed Software Services Agreement (SSA)
* [ ] Internal Announcement to DoubleGDP
* [ ] External Announcement (get approval from city before publication)
* [ ] Receive email confirmation from city that DoubleGDP can mention city in sprint updates
* [ ] Organise Kick-off meeting with DoubleGDP and City Partner leadership to reaffirm committment
* [ ] [Kick off meeting](https://docs.google.com/presentation/d/1ormM1D6dH6fMjOBk6ApT2-fhVyJq5KBrvkmpnhld-jM/edit#slide=id.gbb78ec7270_0_18) with Implementation Manager for product, engineering and customer success

Collect from Partner City:

* [ ] Branding information 
* [ ] Preferred app URL
* [ ] Administrator user details
* [ ] Initial use cases

Product and Engineering:

* [ ] Confirm use cases 
* [ ] Organise periodic discussions with key stakeholders to gather needs
* [ ] Develop training plan and launch materials
* [ ] Set up users and data in the app
* [ ] Set up local language requirements
* [ ] Install required systems on equipment
* [ ] Develop product roll-out plan for first 6 weeks (duration can be extended if applicable)


Customer Success:

* [ ] Set up financial systems and structures (e.g. credit cards, bank accounts)
* [ ] Buy required equipment for operations (e.g. android phones for guards)
* [ ] Review city's business needs and prepare a success plan 
* [ ] Review success plan with partner city
* [ ] Assign a co-located CSM
* [ ] Establish governance framework with partner city for post-onboarding period


