Here are the steps required to offboard employees:

## Manager TODOs
**Administrative**
- [ ] Conduct exit interview (see suggested questions below)
- [ ] Terminate company credit card and revoke bank access (if applicable)
- [ ] Make sure receipts are uploaded prior to suspending/deleting Google Suite access
- [ ] Delete the employees "hello world" in the handbook (https://handbook.doublegdp.com/handbook/)
- [ ] Remove picture and bio from [our website](https://www.doublegdp.com/about/).
- [ ] Remove employee birthday and work anniversary from Google Calendar.
- [ ] Mark as inactive on the [company milestones](https://docs.google.com/spreadsheets/d/1POhDzesk5fEjiafkWxJmsFnjbY5F_c--0fmFdSiO2vE/edit#gid=0)

<details>
  <summary>
    Exit interview questions
  </summary>

  1. Why are you leaving? Is there anything we could have done to keep you here?
  1. What do you think is working well at DoubleGDP?
  1. What can we do better?
  1. What was the best part of your job?
  1. What was the worst part of your job?
  1. Do you have any feedback for your manager or for the CEO?
  1. On a scale of 1-10, how likely are you to recommend DoubleGDP as a place to work?
  1. Anything else you’d like to share?
</details>

## System Administrator TODOs
For the system administrator to complete. (Manager: please assign to either CEO or EA.)
- [ ] Remove from Google Suite, and any group membership within (e.g. "eng" or "csm")
     - _If there are legal issues pending with the teammate, Suspend rather than Delete the user._
     - _If there are no legal issues, reset the password and save it to the Management Vault in 1Password. That teammate's manager should make the decision on whether to keep any documents or data from the account._
- [ ] Remove from GitLab - use "Maintainer" role under the "DoubleGDP" Group by default
- [ ] Remove Slack access
- [ ] [Google Marketing Platform](https://marketingplatform.google.com/home/orgs?authuser=1) -- handles Google Analytics and Tag Manager
- [ ] Remove from Pilot
- [ ] Remove from DGDP[YouTube](https://www.youtube.com/channel/UCALY7l5iisNVrEyvLgQa3ig)
- [ ] Remove from [OnePassword](https://doublegdp.1password.com/)
    - [ ] Double check individual vault access
- [ ] Remove [Zoom](https://zoom.us) (if appliable) - use "Basic" account for engineers, "Licensed" account for teammates who we expect to host meetings frequently
- [ ] Remove from Expensify (if applicable) - only remove if there are no pending transactions/expense reports
- [ ] Remove from [AirTable CRM](https://airtable.com/invite/l?inviteId=invZ8AGEsyzttooGu&inviteToken=9a51ce3c8881f4d03e07707654a7cead06c16e061133db859934848b515f20fe) (if applicable)
- [ ] Remove from Loom's account
- [ ] Remove from Pipedrive
- [ ] Remove from Carta (Stock Options, if applicable)



### Engineering team specific items
- [ ] Remove from Development Environment
    - [ ] Digital Ocean
    - [ ] Google SSO
    - [ ] Facebook SSO
    - [ ] Certficate SSL
    - [ ] CloudflareDNS
- [ ] Remove from Rollbar

## Considerations
- Do any items need to be returned? e.g. [laptop](https://handbook.doublegdp.com/people-group/offboarding/#company-assets-laptop) -- items purchased that are over the threshold where company owns them
any legal needs to cover?
