This tracks the business justification used for a new program or additional spend to an exisiting program. 

## Summary
    <Outline of what is being done; why it is being done and the approval being sought> 

## Current Process
    <What is currently being done; who are the people involved and the information they need to move things forward?>

## Reason to Change/Benefits
    <Benefits that would be realised from making the change>

## Options 
    <List of options available. Include option's cost and comparison among options>

## Risks
    <Risks of undertaking the change>

## Recommendations 
    <Justification author's opinion on the best option>
