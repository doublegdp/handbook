The purpose of the RCCA is to eliminate the cause of the detected nonconformity and permanently resolve the problem(s) it causes.

## Identification
- [ ] Introduce the issue
- [ ] Record basic details.
- [ ] Identify the problem(s)
- [ ] Brainstorm possible causal factors.
- [ ] Identify the root cause(s)
- [ ] Identify communication challenge(s)
- [ ] Identify corrective measure(s)

## Specifics
- [ ] What occurred that is triggering us to have an RCCA?
- [ ] Who is the team that will be involved in the RCCA? Who is leading?
- [ ] What is the timeline of relevant events that occurred leading up to the incident?

## Solution
- [ ] What are the root cause(s) that led us not to deliver the outcomes we would want?
- [ ] What corrective actions does the team recommend? (Should be a list of GitLab issues that can be tracked to completion.)
- [ ] Does the team have confidence that the corrective actions are sufficient to avoid a similar occurrence? (This requires some judgment…) 