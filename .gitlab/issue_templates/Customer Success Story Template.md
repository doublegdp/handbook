**Purpose**
<The aim of the customer succsess story in terms of the product area and the intended market segment for example gate accesss for gated communities>

**Background**
<Provide details about the customer and their buisiness and the reason they have invested in technology to support processes.>

**Challenge**
<Provide details of the specific challenge they aimed to address that is part of the success story. A few examples: the city wanted to increase revenue collection; improve customer satisfaction; increase efficiency in a process.>

**Solution**
<The functionality in the DGDP app used to address the challenge and the how the customer used the functionality. For example, the customer used the payment feature to track revenue collection and send reminders to defaulters>

**Benefit**
<Provide statistics that show the imapact the solution had to the business. For example, after implementing the payment feature the customer reduced the number of defaulters by 10%>

**Customer Quote**
<A quote directly from a customer executive/manager/user about the benefit they realised from the experience using the app>

**Approval Required**
<Yes/No. If approval is required from the customer before publishing the story ensure that their details are ommitted.>
